package com.chemgenerator.images.attach;

import com.chemgenerator.model.ImageCategory;

/**
 * @author Ivan
 * @since 29.08.2016
 */
public interface AttachImageServicesFactory {

    AttachImageService getAttachImageService(ImageCategory imageCategory);
}

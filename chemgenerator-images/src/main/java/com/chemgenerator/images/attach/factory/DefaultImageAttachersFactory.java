package com.chemgenerator.images.attach.factory;

import static com.chemgenerator.images.attach.AttachConstants.AVATAR_ATTACH_SERVICE_BEAN_NAME;

import com.chemgenerator.images.attach.AttachImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.chemgenerator.model.ImageCategory;
import com.chemgenerator.images.attach.AttachImageServicesFactory;
import com.chemgenerator.images.attach.user.AvatarAttachService;

/**
 * @author Ivan
 * @since 30.08.2016
 */
@Service
public class DefaultImageAttachersFactory implements AttachImageServicesFactory {

    /**
     * Class level logger.
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(DefaultImageAttachersFactory.class);


    @Qualifier(AVATAR_ATTACH_SERVICE_BEAN_NAME)
    @Autowired
    private AvatarAttachService avatarAttachService;

    @Override
    public AttachImageService getAttachImageService(ImageCategory imageCategory) {
        AttachImageService service = null;
        if (imageCategory != null) {
            switch (imageCategory) {
                case AVATAR:
                    service = avatarAttachService;
                    break;
                default: {
                    LOGGER.error("No attach service found for: " + imageCategory);
                    service = null;
                }
            }
        }
        return service;
    }
}

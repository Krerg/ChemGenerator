package com.chemgenerator.images.attach.user;

import static com.chemgenerator.images.attach.AttachConstants.AVATAR_ATTACH_SERVICE_BEAN_NAME;

import com.chemgenerator.images.attach.AttachImageService;
import com.chemgenerator.images.exception.AttachImageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.chemgenerator.mongodb.enitites.User;
import com.chemgenerator.mongodb.repos.UserRepository;
import com.chemgenerator.rest.pojo.response.UserInformation;

/**
 * @author Ivan
 * @since 29.08.2016
 */
@Component(value = AVATAR_ATTACH_SERVICE_BEAN_NAME)
public class AvatarAttachService implements AttachImageService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void attachImage(String referenceId, String imagePath) throws AttachImageException {
        String userName = ((UserInformation) SecurityContextHolder.getContext().getAuthentication().getDetails()).getUserLogin();
        if (!userName.equals(referenceId)) {
            // TODO find better solution
            throw new AttachImageException("You can't change not your own avatar!");
        }
        // TODO A. Mylnikov wtf with user creations?
        User userEntity = userRepository.findByLogin(userName);
        userEntity.setImagePath(imagePath);
        userRepository.save(userEntity);
    }

    @Override
    public void deleteAttachment(String referenceId, String imagePath) throws AttachImageException {
        // TODO implement
    }
}

package com.chemgenerator.controllers.support;

import com.chemgenerator.Util.SimpleResponseUtil;
import com.chemgenerator.controllers.ControllerConstants;
import com.chemgenerator.mail.MailService;
import com.chemgenerator.rest.pojo.request.SupportMessage;
import com.chemgenerator.rest.pojo.response.SimpleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = ControllerConstants.SUPPORT_CONTROLLER_PATH)
public class SupportController {

    @Autowired
    private MailService mailService;

    @RequestMapping(value = "/message", method = RequestMethod.POST)
    @ResponseBody
    public SimpleResponse postMessage(@RequestBody SupportMessage message, BindingResult result) {
        mailService.sendMessage(message.messageType, message.message);
        return SimpleResponseUtil.getGoodResponse();
    }

}

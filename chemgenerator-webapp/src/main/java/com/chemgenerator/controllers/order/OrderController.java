package com.chemgenerator.controllers.order;

import com.chemgenerator.controllers.ControllerConstants;
import com.chemgenerator.mongodb.enitites.ProvideTasksMessage;
import com.chemgenerator.mongodb.repos.UserOrderRepository;
import com.chemgenerator.rest.pojo.response.UserOrdersResponse;
import com.chemgenerator.security.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Controller for managing user's queues.
 *
 * @author amylnikov
 */
@RestController
@RequestMapping(value = ControllerConstants.ORDER_CONTROLLER_PATH)
public class OrderController {

    @Autowired
    private UserOrderRepository userOrderRepository;

    @Autowired
    @Qualifier(value = "provideTaskMessageToUserOrderConverter")
    private Converter<ProvideTasksMessage, UserOrdersResponse.Order> userOrderConverter;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public UserOrdersResponse getUserOrders() {
        UserOrdersResponse response = new UserOrdersResponse();
        Collection<ProvideTasksMessage> userOrders = userOrderRepository
                .findByField("userLogin", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getDetails()).getUserInformation().getUserLogin());
        if(!CollectionUtils.isEmpty(userOrders)) {
            userOrders.forEach( message -> {
                response.getUserOrders().add(userOrderConverter.convert(message));
            });
        }
        return response;
    }

}

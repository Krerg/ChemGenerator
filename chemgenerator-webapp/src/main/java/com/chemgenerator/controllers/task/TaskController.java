package com.chemgenerator.controllers.task;

import com.chemgenerator.Util.SimpleResponseUtil;
import com.chemgenerator.config.ConfigCV;
import com.chemgenerator.config.ConfigService;
import com.chemgenerator.controllers.ControllerConstants;
import com.chemgenerator.mongodb.enitites.AvailableTaskEntity;
import com.chemgenerator.mongodb.repos.FileStorage;
import com.chemgenerator.rest.pojo.request.ProvideTasksPojo;
import com.chemgenerator.rest.pojo.response.AvailableTasksResponse;
import com.chemgenerator.rest.pojo.response.SimpleResponse;
import com.chemgenerator.service.TaskService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * Rest controller for tasks. (Obtain new tasks, get available tasks)
 *
 * @author amylnikov
 */
@RestController
@RequestMapping(value = ControllerConstants.TASK_MAIN_CONTROLLER_PATH)
public class TaskController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    private TaskService taskService;

    @Autowired
    private ConfigService configService;

    @Autowired
    private FileStorage fileStorage;

    @Autowired
    @Qualifier("availableTaskEntityToResponseConverter")
    private Converter<List<AvailableTaskEntity>, AvailableTasksResponse> availableTaskEntityToResponseConverter;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public AvailableTasksResponse getAvailableTaskGroups() {
        List<AvailableTaskEntity> availableTasks = taskService.getAvailableTasks();
        AvailableTasksResponse tasksResponse = availableTaskEntityToResponseConverter.convert(availableTasks);
        return tasksResponse;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/provide")
    public SimpleResponse provideTasksToUser(@RequestBody ProvideTasksPojo request) {
        taskService.submitOrder((String)SecurityContextHolder.getContext().getAuthentication().getPrincipal(),
                request.getProvideTaskList(),
                request.getNumberOfCopies(),
                request.getBundleName());
        return SimpleResponseUtil.getGoodResponse("Ваша заявка прината");
    }

    /**
     * Gets file by its name.
     * Returns null if it doesn't exists
     *
     * TODO check for user
     *
     * @param fileName file's name
     * @param response http response
     */
    @RequestMapping(value = "/task_bundles/{file_name}", method = RequestMethod.GET)
    public void getTaskBundle(@PathVariable("file_name") String fileName, HttpServletResponse response) {
        try {
            response.addHeader("Content-Type", "content/unknown");
            byte[] fileData = fileStorage.get(fileName+".docx");
            response.getOutputStream().write(fileData);
            response.flushBuffer();
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage());
        }
    }

}

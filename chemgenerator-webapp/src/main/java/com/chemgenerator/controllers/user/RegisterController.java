package com.chemgenerator.controllers.user;

import com.chemgenerator.controllers.ControllerConstants;
import com.chemgenerator.mongodb.enitites.UserTaskBundles;
import com.chemgenerator.mongodb.repos.TaskBundleRepository;
import com.chemgenerator.rest.pojo.response.ErrorMessage;
import com.chemgenerator.security.Authorities;
import com.chemgenerator.rest.pojo.response.UserInformation;
import com.chemgenerator.security.UserDetails;
import com.chemgenerator.service.TaskService;
import com.mongodb.DuplicateKeyException;
import org.springframework.beans.factory.annotation.Qualifier;

import com.chemgenerator.mongodb.enitites.User;
import com.chemgenerator.mongodb.repos.UserRepository;
import com.chemgenerator.rest.pojo.response.UserPojo;
import com.chemgenerator.rest.pojo.response.UserResponse;
import com.chemgenerator.rest.status.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author amylnikov
 */
@RestController
@RequestMapping(value = ControllerConstants.USER_CONTROLLER_PATH)
public class RegisterController {

    /**
     * Converter from request param to DB entity.
     */
    @Autowired
    @Qualifier(value = "userPojoToEntityMapper")
    private Converter<UserPojo, User> userPojoToEntityConverter;

    @Autowired
    @Qualifier(value = "userPojoValidator")
    private Validator userPojoValidator;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TaskBundleRepository taskBundleRepository;

    @Autowired
    private TaskService taskService;

    @Autowired
    @Qualifier(value = "userEntityToUserInformationConverter")
    private Converter<User, UserInformation> converter;

    /**
     * Register new user (persist in DB and provide permissions).
     * @return operation status
     */
    @RequestMapping(method = RequestMethod.POST, value = "/register")
    public UserResponse register(@RequestBody UserPojo user, BindingResult result) {
        userPojoValidator.validate(user,result);
        if(result.hasErrors()){
            return createErrorUserResponse(result);
        }
        User newUser = userPojoToEntityConverter.convert(user);
        try {
            taskBundleRepository.save(newUser.getUserBundles());
            userRepository.save(newUser);
            newUser.getUserBundles().setUser(newUser);
            taskBundleRepository.save(newUser.getUserBundles());
        } catch (DuplicateKeyException dke) {
            return userExistsResponse();
        }
        grantUserPermission(newUser);
        return createSuccessUserResponse();
    }

    /**
     * Registers test user. User's IP used as login, password generated randomly.
     * @param request register request, contains user's IP
     * @return operation status (always success if there is no problem with DB)
     */
    @RequestMapping(method = RequestMethod.POST, value = "/register/test")
    public UserResponse registerTestUser(HttpServletRequest request) {
        User user = userRepository.findByLogin(request.getRemoteAddr());
        if(user==null) {
            user = createEntityForTestUser(request.getRemoteAddr());
            taskBundleRepository.save(user.getUserBundles());
            userRepository.save(user);
        }
        grantUserPermission(user);
        return createSuccessUserResponse();
    }

    /**
     * Creates entity for test user
     * @param userIp user's IP which will be used as his login
     * @return Entity for test user
     */
    private static User createEntityForTestUser(String userIp) {
        User testUser = new User();
        testUser.setName("Test");
        testUser.setEmail("Test email");
        testUser.setBirth(new Date());
        testUser.setLogin(userIp);
        testUser.setUserBundles(new UserTaskBundles());
        return testUser;
    }

    private UserResponse userExistsResponse() {
        UserResponse userResponse = new UserResponse();
        userResponse.setStatus(Status.ERROR);
        userResponse.getErrors().add(new ErrorMessage("login", "Пользователь с таким логином уже существует"));
        return userResponse;
    }

    public UserResponse createSuccessUserResponse() {
        UserResponse userResponse = new UserResponse();
        userResponse.setStatus(Status.OK);
        return userResponse;
    }

    public UserResponse createErrorUserResponse(BindingResult result) {
        UserResponse userResponse = new UserResponse();
        userResponse.setStatus(Status.ERROR);
        ArrayList<ErrorMessage> list = new ArrayList<>();
        for (ObjectError error :result.getAllErrors())
            list.add(new ErrorMessage(error.getCodes()[1],error.getDefaultMessage()));
        userResponse.setErrors(list);
        return userResponse;
    }

    /**
     * Grants user permission {@link Authorities#USER} to current context
     */
    private void grantUserPermission(User user) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(Authorities.USER.name()));
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user.getLogin(), user.getPassword(), authorities);
        UserInformation userInformation = converter.convert(user);
        UserDetails userDetails = new UserDetails();
        userDetails.setUserInformation(userInformation);
        authentication.setDetails(userDetails);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

}

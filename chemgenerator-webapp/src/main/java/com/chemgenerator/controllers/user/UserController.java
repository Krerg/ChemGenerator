package com.chemgenerator.controllers.user;

import com.chemgenerator.controllers.ControllerConstants;
import com.chemgenerator.rest.pojo.response.*;
import com.chemgenerator.security.UserDetails;
import com.chemgenerator.services.impl.UserServiceImpl;
import com.chemgenerator.mongodb.enitites.User;
import com.chemgenerator.rest.status.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * Controller for user operations, such as get information (name, email, etc.),
 * update user information, upload new order.
 *
 * @author Ivan
 * @author Aleksandr Mylnikov
 * @since 11.06.2016
 */
@RestController
@RequestMapping(value = ControllerConstants.USER_CONTROLLER_PATH)
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    /**
     * Converter from Morphia entity to response.
     */
    @Qualifier(value = "userEntityToUserResponseConverter")
    private Converter<User, UserInformationResponse> userEntityToPojoConverter;

    /**
     * Gets information about user (name, surname etc.).
     * <p>
     * NOTE! No user id must be sended, because we can get user by jsessionid.
     *
     * @return user's information.
     */
    @RequestMapping(method = RequestMethod.GET, value = "/")
    public UserInformation getInfo() {
        return ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getDetails()).getUserInformation();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/")
    public UserResponse updateUser(@RequestBody UserPojo user) {
        userService.updateUser(user);
        return getUserResponse(Status.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/bundles")
    public UserTaskBundlesResponse getBundles() {
        UserTaskBundlesResponse response = new UserTaskBundlesResponse();
        response.setBundles(userService.getUserTaskBundles());
        return response;
    }

    /**
     * Creates response for operation with given status and user.
     * @param status
     * @return operation response
     */
    private UserResponse getUserResponse(Status status) {
        UserResponse userResponse = new UserResponse();
        userResponse.setStatus(status);
        return userResponse;
    }

}

package com.chemgenerator.controllers;

import com.chemgenerator.mongodb.enitites.TaskEntity;
import com.chemgenerator.mongodb.repos.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author amylniko
 */
@RestController
@RequestMapping(value = "/testOutput")
public class TestOutputController {

    @Autowired
    private TaskRepository taskRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public String getTestOutPut() {
        TaskEntity task = taskRepository.findAll().get(0);
        String responseString = "<html><head>\n" +
                "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                "  <title>Таблица</title>\n" +
                "  <style type=\"text/css\">\n" +
                "   TABLE {\n" +
                "    width: 300px; /* Ширина таблицы */\n" +
                "    border-collapse: collapse; /* Убираем двойные линии между ячейками */\n" +
                "   }\n" +
                "   TD, TH {\n" +
                "    padding: 3px; /* Поля вокруг содержимого таблицы */\n" +
                "    border: 1px solid black; /* Параметры рамки */\n" +
                "   }\n" +
                "   TH {\n" +
                "    background: #b0e0e6; /* Цвет фона */\n" +
                "   }\n" +
                "  </style>\n" +
                " </head>";
        responseString+=task.getCondition();
        responseString+="<body><table style='border-collapse: collapse'>";
        for(List<String> row: task.getTaskTable()) {
            responseString+="<tr>";
            for(String column : row) {
                responseString+="<td>";
                responseString+=column;
                responseString+="</td>";
            }
            responseString+="</tr>";
        }
        responseString+="</table></body></html>";
        return responseString;
    }

}

package com.chemgenerator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import javax.annotation.PostConstruct;

@SpringBootApplication
@ComponentScan(basePackages = {"com"})
@EnableWebSecurity
@EnableAutoConfiguration(exclude={MongoAutoConfiguration.class})
public class ChemGeneratorApplication  {

    private static Logger LOGGER = LoggerFactory.getLogger(ChemGeneratorApplication.class);

    @PostConstruct
    public void printEnvironemntInfo() {
        String OS = System.getProperty("os.name");
        LOGGER.info("System OS: " + OS);
    }

    public static void main(String[] args) {
        SpringApplication.run(ChemGeneratorApplication.class, args);
    }

}
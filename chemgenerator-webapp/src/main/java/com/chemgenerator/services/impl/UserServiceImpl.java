package com.chemgenerator.services.impl;

import com.chemgenerator.mongodb.enitites.TaskBundle;
import com.chemgenerator.mongodb.enitites.User;
import com.chemgenerator.mongodb.enitites.UserTaskBundles;
import com.chemgenerator.mongodb.repos.UserRepository;
import com.chemgenerator.rest.pojo.response.UserInformation;
import com.chemgenerator.rest.pojo.response.UserPojo;
import com.chemgenerator.security.UserDetails;
import com.chemgenerator.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

/**
 * @{inheritDoc}
 */
@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    /**
     * Searches user in repository by given login.
     *
     * @param login user's login
     * @return user if given login exists, otherwise null.
     */
    public User findUserByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    /**
     * {@inheritDoc}
     */
    public void updateUser(UserPojo userPojo) {
        if (userPojo.getLogin() != null) {
            User user;
            if (null != (user = findUserByLogin(userPojo.getLogin()))) {
                updateUserEntityFields(user, userPojo);
                userRepository.save(user);
                updateUserInformation(userPojo);
            }
        }
    }

    @Override
    public List<String> getUserTaskBundles() {
        String userName = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserTaskBundles userTaskBundles = userRepository.findByLogin(userName).getUserBundles();
        List<String> userTaskBundlesFiles = new LinkedList<>();
        for(TaskBundle taskBundle : userTaskBundles.getTaskBundles()) {
            userTaskBundlesFiles.add(taskBundle.getBundleFileNameWithAnswers());
            userTaskBundlesFiles.add(taskBundle.getBundleFileNameWithoutAnswers());
        }
        return userTaskBundlesFiles;
    }

    private void updateUserInformation(UserPojo userPojo) {
        UserInformation userInformation = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getDetails()).getUserInformation();
        if (userPojo.getName() != null) userInformation.setName(userPojo.getName());
        if (userPojo.getEmail() != null) userInformation.setEmail(userPojo.getEmail());
        if (userPojo.getBirth() != null) userInformation.setBirth(userPojo.getBirth());
        if (userPojo.getLogin() != null) userInformation.setUserLogin(userPojo.getLogin());
    }

    private void updateUserEntityFields(User userEntity, UserPojo userPojo) {
        if (userPojo.getPassword() != null) userEntity.setPassword(userPojo.getPassword());
        if (userPojo.getEmail() != null) userEntity.setEmail(userPojo.getEmail());
        if (userPojo.getName() != null) userEntity.setName(userPojo.getName());
        if (userPojo.getBirth() != null) userEntity.setBirth(userPojo.getBirth());
    }
}

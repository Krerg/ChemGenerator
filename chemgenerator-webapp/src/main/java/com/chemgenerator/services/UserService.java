package com.chemgenerator.services;

import com.chemgenerator.rest.pojo.response.UserPojo;

import java.util.List;

/**
 * Service for updating and getting information about current user
 */
public interface UserService {

    /**
     * Updates current session user.
     * @param userPojo new information
     */
    void updateUser(UserPojo userPojo);

    List<String> getUserTaskBundles();


}

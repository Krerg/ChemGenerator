package com.chemgenerator.mapping;

import com.chemgenerator.mongodb.enitites.User;
import com.chemgenerator.mongodb.enitites.UserTaskBundles;
import com.chemgenerator.rest.pojo.response.UserPojo;
import com.chemgenerator.security.HashUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component(value = "userPojoToEntityMapper")
public class UserPojoToEntityMapper implements Converter<UserPojo,User> {

    @Override
    public User convert(UserPojo userPojo) {
        User entity = new User();
        entity.setLogin(userPojo.getLogin());
        entity.setName(userPojo.getName());
        entity.setEmail(userPojo.getEmail());
        entity.setBirth(userPojo.getBirth());
        entity.setPassword(HashUtils.hash(userPojo.getPassword()));
        UserTaskBundles userTaskBundles = new UserTaskBundles();
        entity.setUserBundles(userTaskBundles);
        return entity;
    }
}

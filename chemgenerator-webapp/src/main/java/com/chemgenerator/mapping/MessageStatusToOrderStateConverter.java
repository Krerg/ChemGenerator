package com.chemgenerator.mapping;

import com.chemgenerator.mongodb.queue.MessageStatus;
import com.chemgenerator.rest.pojo.response.UserOrdersResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author amylnikov
 */
@Component(value = "messageStatusToOrderStateConverter")
public class MessageStatusToOrderStateConverter implements Converter<MessageStatus, UserOrdersResponse.OrderState> {

    @Override
    public UserOrdersResponse.OrderState convert(MessageStatus messageStatus) {
        switch (messageStatus) {
            case PROCESSING:
                return UserOrdersResponse.OrderState.PROCESSING;
            case READY:
                return UserOrdersResponse.OrderState.IN_QUEUE;
            default:
                throw new UnsupportedOperationException();
        }
    }

}

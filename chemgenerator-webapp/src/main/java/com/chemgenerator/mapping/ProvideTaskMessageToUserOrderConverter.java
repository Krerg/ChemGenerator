package com.chemgenerator.mapping;

import com.chemgenerator.mongodb.enitites.ProvideTasksMessage;
import com.chemgenerator.mongodb.queue.MessageStatus;
import com.chemgenerator.rest.pojo.response.UserOrdersResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author amylnikov
 */
@Component(value = "provideTaskMessageToUserOrderConverter")
public class ProvideTaskMessageToUserOrderConverter implements Converter<ProvideTasksMessage, UserOrdersResponse.Order> {

    @Autowired
    @Qualifier(value = "messageStatusToOrderStateConverter")
    private Converter<MessageStatus, UserOrdersResponse.OrderState> orderStateConverter;

    @Override
    public UserOrdersResponse.Order convert(ProvideTasksMessage provideTasksMessage) {
        UserOrdersResponse.Order order = new UserOrdersResponse.Order();
        order.orderId = provideTasksMessage.getObjectId().toString();
        order.orderState = orderStateConverter.convert(provideTasksMessage.getMessageStatus());
        order.bundleName = provideTasksMessage.getBundleName();
        return order;
    }
}

package com.chemgenerator.mapping;

import com.chemgenerator.mongodb.enitites.AvailableTaskEntity;
import com.chemgenerator.rest.pojo.response.AvailableTasksResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author amylnikov
 */
@Component(value = "availableTaskEntityToResponseConverter")
public class AvailableTaskEntityToResponseConverter implements Converter<List<AvailableTaskEntity>,AvailableTasksResponse> {

    @Override
    public AvailableTasksResponse convert(List<AvailableTaskEntity> availableTaskEntities) {
        AvailableTasksResponse response = new AvailableTasksResponse();
        response.setAvailableTasks(new LinkedList<>());
        for(AvailableTaskEntity entity : availableTaskEntities) {
            AvailableTasksResponse.Task task = new AvailableTasksResponse.Task(entity.getGroupVariant(),entity.getTaskVariants());
            response.getAvailableTasks().add(task);
        }
        return response;
    }

}

package com.chemgenerator.security;

import com.chemgenerator.cv.Profiles;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * @author amylnikov
 */
@Component
@Profile(value = {Profiles.PROD})
public class CorsConfigProd {

    @Bean(value = "origin")
    public String origin() {
        return "https://chemgen.herokuapp.com";
    }

}

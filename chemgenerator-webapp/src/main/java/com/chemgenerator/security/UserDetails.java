package com.chemgenerator.security;

import com.chemgenerator.rest.pojo.response.UserInformation;

/**
 * User details which stired in Spring Security session.
 * Contains user (not entity, just pojo)
 *
 * @author amylniko
 */
public class UserDetails  {

    private UserInformation userInformation;

    public UserInformation getUserInformation() {
        return userInformation;
    }

    public void setUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;
    }
}

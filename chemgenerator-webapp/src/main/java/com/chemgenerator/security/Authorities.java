package com.chemgenerator.security;

/**
 * Created by amylniko on 28.07.2016.
 */
public enum Authorities {

    USER("user"), ADMIN("admin"), INTERNAL("internal"), TEST_USER("test");
    String authority;

    Authorities(String authority) {
        this.authority = authority;
    }
}

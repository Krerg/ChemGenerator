package com.chemgenerator.security;

import com.chemgenerator.cv.Profiles;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * @author amylnikov
 */
@Component
@Profile(value = {Profiles.DEV})
public class CorsConfigDev {

    @Bean(value = "origin")
    public String origin() {
        return "http://localhost:4200";
    }

}

package com.chemgenerator.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author amylnikov
 */
public class HashUtils {

    private static final String HASH_ALGORITHM = "SHA-256";

    private static final Logger LOGGER = LoggerFactory.getLogger(HashUtils.class);

    public static String hash(String input) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance(HASH_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Exception while initialization MessageDigest");
            throw new RuntimeException("Cannot initialize MessageDigest");
        }
        byte[] hash = digest.digest(input.getBytes());
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

}

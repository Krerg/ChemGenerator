package com.chemgenerator.security.cors;

import com.chemgenerator.cv.Profiles;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author amylnikov
 */
@Configuration
@Profile(Profiles.DEV)
public class CorsDevConfig {

    @Bean(value = "allowedOrigin")
    public String allowedOrigin() {
        return "http://localhost:4200";
    }

}

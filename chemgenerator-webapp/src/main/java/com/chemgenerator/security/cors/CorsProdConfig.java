package com.chemgenerator.security.cors;

import com.chemgenerator.cv.Profiles;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author amylnikov
 */
@Configuration
@Profile(Profiles.PROD)
public class CorsProdConfig {

    @Bean(value = "allowedOrigin")
    public String allowedOrigin() {
        return "https://chemgen.herokuapp.com";
    }

}

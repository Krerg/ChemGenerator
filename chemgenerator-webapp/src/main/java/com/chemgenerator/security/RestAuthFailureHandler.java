package com.chemgenerator.security;

import com.chemgenerator.rest.pojo.response.ErrorMessage;
import com.chemgenerator.rest.pojo.response.UserResponse;
import com.chemgenerator.rest.status.Status;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.Writer;

public class RestAuthFailureHandler implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        HttpServletResponseWrapper responseWrapper = new HttpServletResponseWrapper(httpServletResponse);
        Writer out = responseWrapper.getWriter();
        ObjectMapper objectMapper = new ObjectMapper();
        UserResponse badResponse = new UserResponse();
        badResponse.setStatus(Status.ERROR);
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setError(e.getMessage());
        badResponse.getErrors().add(errorMessage);
        out.write(objectMapper.writeValueAsString(badResponse));
        out.close();
    }
}

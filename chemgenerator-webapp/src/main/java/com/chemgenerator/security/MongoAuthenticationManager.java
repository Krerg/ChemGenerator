package com.chemgenerator.security;

import com.chemgenerator.mongodb.enitites.User;
import com.chemgenerator.mongodb.repos.UserRepository;
import com.chemgenerator.rest.pojo.response.UserInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Authentication manager for Spring Security.
 * Authenticate by MongoDB (collection 'User')
 *
 * @see UserRepository
 * @see User
 *
 */
@Component
public class MongoAuthenticationManager implements AuthenticationProvider {

    @Autowired
    @Qualifier(value = "userEntityToUserInformationConverter")
    private Converter<User, UserInformation> converter;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        User user = userRepository.findByLogin((String) authentication.getPrincipal());
        if(user==null) {
            throw new BadCredentialsException("Неправильный логин/пароль");
        } else {
            String hashedPass = HashUtils.hash((String)authentication.getCredentials());
            if(!user.getPassword().equals(hashedPass))
                throw new BadCredentialsException("Неправильный логин/пароль");
        }
        UserInformation userInformation = converter.convert(user);
        ((UserDetails)authentication.getDetails()).setUserInformation(userInformation);
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(Authorities.USER.authority));
        return new UsernamePasswordAuthenticationToken(authentication.getPrincipal(),authentication.getCredentials(),authorities);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        if(aClass.equals(UsernamePasswordAuthenticationToken.class)) {
            return true;
        }
        return false;
    }
}

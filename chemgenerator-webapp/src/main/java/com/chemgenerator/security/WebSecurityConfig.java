package com.chemgenerator.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.Arrays;

/**
 * @author amylnikov
 */
@Configuration
@EnableGlobalMethodSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private MongoAuthenticationManager mongoAuthenticationManager;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CustomTokenAuthenticationFilter filter = new CustomTokenAuthenticationFilter();
        filter.setAuthenticationSuccessHandler(new RestAuthSuccessHandler());
        filter.setAuthenticationFailureHandler(new RestAuthFailureHandler());
        filter.setPostOnly(true);
        filter.setFilterProcessesUrl("/j_spring_security_check");
        filter.setAuthenticationManager(this.getAuthenticationManager());
        http
                .addFilterBefore(filter, UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers("/rest/**/register/**")
                .permitAll()
                .antMatchers("/rest/**").authenticated()
                .antMatchers("/images/**").permitAll()
                .and()
                .logout()
                .logoutUrl("/logout")
                .invalidateHttpSession(true)
                .and()
                .csrf()
                .disable();
    }

    protected AuthenticationManager getAuthenticationManager() throws Exception {
        AuthenticationManager authManager = new ProviderManager(Arrays.asList(mongoAuthenticationManager));
        return authManager;
    }
}

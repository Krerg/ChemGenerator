package com.chemgenerator.security;

import com.chemgenerator.rest.pojo.response.UserResponse;
import com.chemgenerator.rest.status.Status;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.Writer;

public class RestAuthSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        HttpServletResponseWrapper responseWrapper = new HttpServletResponseWrapper(httpServletResponse);
        Writer out = responseWrapper.getWriter();
        ObjectMapper objectMapper = new ObjectMapper();
        UserResponse response = new UserResponse();
        response.setStatus(Status.OK);
        out.write(objectMapper.writeValueAsString(response));
        out.close();
    }
}

package com.chemgenerator.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Pattern;

/**
 * Created by amylniko on 26.07.2016.
 */
@Component(value = "emailValidator")
public class EmailValidator implements Validator {

    private static Pattern emailPattern = Pattern.compile(
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(String.class);
    }

    @Override
    public void validate(Object target, Errors errors) {

        String email = (String) target;

        if(!emailPattern.matcher(email).matches()){
            errors.rejectValue("email", null, null,ValidationMessages.INVALID_EMAIL_ERROR);
        }
    }
}

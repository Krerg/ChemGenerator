package com.chemgenerator.validation;

import org.springframework.stereotype.Component;

/**
 * Created by Daimo on 12.12.2016.
 */
@Component(value = "validationMessages")
public class ValidationMessages {
    public static final String LOGIN_SIZE_ERROR = "Логин не должен превышать " +
            ValidationProperties.LOGIN_MAX_SIZE + " сиволов.";

    public static final String NAME_SIZE_ERROR = "Имя не должно превышать " +
            ValidationProperties.NAME_MAX_SIZE + " символов.";

    public static final String SURNAME_SIZE_ERROR = "Фимилия не должна превышать " +
            ValidationProperties.SURNAME_MAX_SIZE + "  символов.";

    public static final String INVALID_PASSWORD_ERROR = "Пароль должен быть от " +
            ValidationProperties.PASSWORD_MIN_SIZE + "" +
            " до " + ValidationProperties.PASSWORD_MAX_SIZE + " символов.";

    public static final String INVALID_EMAIL_ERROR = "Введите коректный e-mail адрес.";

    public static final String EMAIL_SIZE_ERROR = "E-mail адрес не должен превышать " +
            ValidationProperties.EMAIL_MAX_SIZE + " символов.";

    public static final String REQUIRED_FIELD_ERROR = "Поле обязательно для заполнения.";
}

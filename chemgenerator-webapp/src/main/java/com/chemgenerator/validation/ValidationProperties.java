package com.chemgenerator.validation;

/**
 * Created by Daimo on 14.12.2016.
 */
public class ValidationProperties {
    public static final int LOGIN_MAX_SIZE = 40;

    public static final int NAME_MAX_SIZE = 40;

    public static final int SURNAME_MAX_SIZE = 40;

    public static final int PASSWORD_MIN_SIZE = 5;

    public static final int PASSWORD_MAX_SIZE = 40;

    public static final int EMAIL_MAX_SIZE = 40;
}

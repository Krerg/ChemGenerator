package com.chemgenerator.validation;

import com.chemgenerator.rest.pojo.response.UserPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import javax.validation.Valid;

/**
 * Validator for user pojo {@link UserPojo}.
 * Validates:
 * <ul>
 * <li>{@link UserPojo#login} must me less than 40 characters and unique (constraint in DB)</li>
 * <li>{@link UserPojo#password} must be less more than 5 and less than 40 characters</li>
 * <li>{@link UserPojo#name} must be less than 40 characters</li>
 * </ul>
 *
 * Also contains validator for email {@link #emailValidator}, that validates {@link UserPojo#email}
 *
 * @author amylniko
 */
@Component(value = "userPojoValidator")
public class UserPojoValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(UserPojo.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        nullValidation(target,errors);
        if(errors.hasErrors()) {
            return;
        }
        UserPojo user = (UserPojo) target;
        sizeValidation(user, errors);

    }

    private void sizeValidation(UserPojo user, Errors errors) {
        if (user.getPassword().length() < ValidationProperties.PASSWORD_MIN_SIZE ||
                user.getPassword().length() > ValidationProperties.PASSWORD_MAX_SIZE)
            errors.rejectValue("password", null, null, ValidationMessages.INVALID_PASSWORD_ERROR);

        if (user.getLogin().length() > ValidationProperties.LOGIN_MAX_SIZE)
            errors.rejectValue("login", null, null, ValidationMessages.LOGIN_SIZE_ERROR);

        if (user.getName() != null && user.getName().length() > ValidationProperties.NAME_MAX_SIZE)
            errors.rejectValue("name", null, null, ValidationMessages.NAME_SIZE_ERROR);

        if (user.getEmail() != null && user.getEmail().length() > ValidationProperties.EMAIL_MAX_SIZE)
            errors.rejectValue("email", null, null, ValidationMessages.EMAIL_SIZE_ERROR);
    }

    private void nullValidation(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", null, null, ValidationMessages.REQUIRED_FIELD_ERROR);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", null, null, ValidationMessages.REQUIRED_FIELD_ERROR);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", null, null, ValidationMessages.REQUIRED_FIELD_ERROR);

    }

}

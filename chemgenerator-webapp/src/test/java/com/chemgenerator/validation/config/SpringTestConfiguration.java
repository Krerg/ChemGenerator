package com.chemgenerator.validation.config;

import com.chemgenerator.validation.EmailValidator;
import com.chemgenerator.validation.UserPojoValidator;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.Validator;

/**
 * Created by amylniko on 30.01.2017.
 */
@Configuration
public class SpringTestConfiguration {

    @Bean(value = "userPojoValidator")
    public Validator getTestValidator() {
        return new UserPojoValidator();
    }

    @Bean(value = "emailValidator")
    public Validator getEmailValidator() {
        return new EmailValidator();
    }

}

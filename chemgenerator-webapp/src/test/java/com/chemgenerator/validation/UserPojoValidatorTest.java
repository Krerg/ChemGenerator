package com.chemgenerator.validation;

import com.chemgenerator.rest.pojo.response.UserPojo;

import com.chemgenerator.validation.config.SpringTestConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;

/**
 * Created by Daimo on 06.01.2017.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringTestConfiguration.class)
public class UserPojoValidatorTest {

    @Autowired
    @Qualifier(value = "userPojoValidator")
    private Validator userPojoValidator;

    public UserPojo createValidUserPojo() {
        UserPojo validUser = new UserPojo();
        try {
            validUser.setBirth(new SimpleDateFormat("yyyy-MM-dd").parse("1995-12-05"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        validUser.setEmail("validMail@email.com");
        validUser.setLogin("validLogin");
        validUser.setPassword("validPassword");
        validUser.setName("validName");

        return validUser;
    }

    @Test
    public void testValidationWithValidAddress() {
        UserPojo validUser = createValidUserPojo();
        Errors errors = new BindException(validUser, "validUser");
        userPojoValidator.validate(validUser, errors);
        assertFalse(errors.hasErrors());
    }
}

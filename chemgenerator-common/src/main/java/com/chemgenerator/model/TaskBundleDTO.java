package com.chemgenerator.model;

/**
 * Created by amylnikov on 20.02.2018.
 */
public class TaskBundleDTO {

    public final String fileWithAnswersName;

    public final String fileWithoutAnswersName;

    public final byte[] fileWithAnswersData;

    public final byte[] fileWithoutAnswersData;

    public TaskBundleDTO(String fileWithAnswersName, String fileWithoutAnswersName, byte[] fileWithAnswersData, byte[] fileWithoutAnswersData) {
        this.fileWithAnswersName = fileWithAnswersName;
        this.fileWithoutAnswersName = fileWithoutAnswersName;
        this.fileWithAnswersData = fileWithAnswersData;
        this.fileWithoutAnswersData = fileWithoutAnswersData;
    }
}

package com.chemgenerator.rest.pojo.response;


import java.util.*;

/**
 * @author amylnikov
 */
public class AvailableTasksResponse {

    private List<Task> availableTasks;

    public List<Task> getAvailableTasks() {
        if (availableTasks == null) {
            availableTasks = new ArrayList<>();
        }
        return availableTasks;
    }

    public void setAvailableTasks(List<Task> availableTasks) {
        this.availableTasks = availableTasks;
    }

    public static class Task {
        public String name;
        public List<String> subTask;

        public Task(String name, List<String> subTask) {
            this.name = name;
            this.subTask = subTask;
        }

        public String getName() {
            return name;
        }

        public List<String> getSubTask() {
            return subTask;
        }
    }
}

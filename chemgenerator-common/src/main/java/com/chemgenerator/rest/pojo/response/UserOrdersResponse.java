package com.chemgenerator.rest.pojo.response;

import java.util.ArrayList;
import java.util.List;

/**
 * @author amylnikov
 *
 */
public class UserOrdersResponse {

    private List<Order> userOrders;

    public static class Order {

        public String orderId;

        public String bundleName;

        public OrderState orderState;

        public String errorMessage;

    }

    public enum OrderState {
        IN_QUEUE, PROCESSING, ERROR;
    }

    public List<Order> getUserOrders() {
        if(userOrders == null)
            userOrders = new ArrayList<>();
        return userOrders;
    }

    public void setUserOrders(List<Order> userOrders) {
        this.userOrders = userOrders;
    }
}

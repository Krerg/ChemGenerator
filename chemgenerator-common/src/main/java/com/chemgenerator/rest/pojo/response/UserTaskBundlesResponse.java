package com.chemgenerator.rest.pojo.response;

import java.util.ArrayList;
import java.util.List;

/**
 * @author amylnikov
 */
public class UserTaskBundlesResponse {


    private List<String> bundles;

    public List<String> getBundles() {
        if(bundles == null) {
            bundles = new ArrayList<>();
        }
        return bundles;
    }

    public void setBundles(List<String> bundles) {
        this.bundles = bundles;
    }
}

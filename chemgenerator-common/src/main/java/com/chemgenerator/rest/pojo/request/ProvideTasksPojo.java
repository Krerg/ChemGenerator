package com.chemgenerator.rest.pojo.request;

import java.util.LinkedList;
import java.util.List;

/**
 * @author amylnikov
 */
public class ProvideTasksPojo {

    private String bundleName;

    private int numberOfCopies;

    private List<ProvideTaskPojo> provideTaskList = new LinkedList<>();

    public List<ProvideTaskPojo> getProvideTaskList() {
        return provideTaskList;
    }

    public void setProvideTaskList(List<ProvideTaskPojo> provideTaskList) {
        this.provideTaskList = provideTaskList;
    }

    public int getNumberOfCopies() {
        return numberOfCopies;
    }

    public void setNumberOfCopies(int numberOfCopies) {
        this.numberOfCopies = numberOfCopies;
    }

    public String getBundleName() {
        return bundleName;
    }

    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }
}

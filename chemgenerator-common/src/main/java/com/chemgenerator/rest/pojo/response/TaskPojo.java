package com.chemgenerator.rest.pojo.response;

import java.util.List;

/**
 * @author amylnikov
 */
public class TaskPojo {

    private String groupVariant;

    private String taskVariant;

    private String condition;

    private List<List<String>> table;

    private String answer;

    public String getGroupVariant() {
        return groupVariant;
    }

    public void setGroupVariant(String groupVariant) {
        this.groupVariant = groupVariant;
    }

    public String getTaskVariant() {
        return taskVariant;
    }

    public void setTaskVariant(String taskVariant) {
        this.taskVariant = taskVariant;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public List<List<String>> getTable() {
        return table;
    }

    public void setTable(List<List<String>> table) {
        this.table = table;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}

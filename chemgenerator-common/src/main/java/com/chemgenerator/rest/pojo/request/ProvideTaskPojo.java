package com.chemgenerator.rest.pojo.request;

/**
 * @author amylnikov
 */
public class ProvideTaskPojo {

    private String groupVariant;

    private String taskVariant;

    public String getGroupVariant() {
        return groupVariant;
    }

    public void setGroupVariant(String groupVariant) {
        this.groupVariant = groupVariant;
    }

    public String getTaskVariant() {
        return taskVariant;
    }

    public void setTaskVariant(String taskVariant) {
        this.taskVariant = taskVariant;
    }
}

package com.chemgenerator.rest.pojo.response;

import com.chemgenerator.rest.status.Status;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by amylniko on 26.07.2016.
 */
public class UserResponse {

    private Status status;

    private List<ErrorMessage> errors = new LinkedList<>();

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<ErrorMessage> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorMessage> errors) {
        this.errors = errors;
    }
}

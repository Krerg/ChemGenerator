package com.chemgenerator.cv;

/**
 * Available profiles
 * @author amylnikov
 */
public class Profiles {

    public static final String DEV = "dev";

    public static final String PROD = "prod";

    public static final String TEST = "test";

    private Profiles() {

    }

}

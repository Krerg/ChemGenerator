package com.chemgenerator.mongodb.repos;

import com.chemgenerator.mongodb.enitites.TaskEntity;
import com.chemgenerator.mongodb.enitites.AvailableTaskEntity;
import org.bson.types.ObjectId;
import org.mongodb.morphia.aggregation.Group;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Repository for tasks (not assigned)
 *
 * @see TaskEntity
 *
 * @author amylnikov
 */
@Repository
public class TaskRepository extends MongoRepository<TaskEntity, ObjectId> {

    /**
     * Operation to mark task. Tasks are marked in order to show,
     * that they are already being processed by something.
     *
     * @see #markAndGetTask(String, String)
     */
    private UpdateOperations<TaskEntity> markTaskOperation;


    /**
     * {@inheritDoc}
     */
    public TaskRepository() {
        super(TaskEntity.class);
    }

    @PostConstruct
    public void init() {
        markTaskOperation = datastore.createUpdateOperations(TaskEntity.class);
        markTaskOperation.set("isMarked", true);
        resetMarkedTasks();
    }

    /**
     * Unmarks all tasks.
     */
    private void resetMarkedTasks() {
        UpdateOperations<TaskEntity> unmarkTaskOperation = datastore.createUpdateOperations(TaskEntity.class);
        unmarkTaskOperation.set("isMarked",false);
        datastore.update(datastore.createQuery(TaskEntity.class), unmarkTaskOperation);
    }

    /**
     * Gets available task's group and task variants.
     *
     * @return list of unique group variants and task variants.
     */
    public List<AvailableTaskEntity> getAvailableTasks() {
        Iterator<AvailableTaskEntity> entities = datastore.createAggregation(TaskEntity.class).group("groupVariant",Group.grouping("taskVariants",Group.addToSet("taskVariant"))).out(AvailableTaskEntity.class);
        List<AvailableTaskEntity> availableTasks = new LinkedList<>();
        entities.forEachRemaining(task -> availableTasks.add(task));
        return availableTasks;
    }

    /**
     * Finds task by given groupVariant and taskVariant and mark it in order to exclude concurrent modification
     * @param groupVariant task's groupVariant {@link TaskEntity#getGroupVariant()}. Cannot be null
     * @param taskVariant task's taskVariant {@link TaskEntity#getTaskVariant()} ()}. Cannot be null
     * @return task with given search parameter if it exists in DB, otherwise null
     */
    public TaskEntity markAndGetTask(String groupVariant, String taskVariant) {
        Query<TaskEntity> queryTaskByGroupAndTaskVariant = datastore.createQuery(TaskEntity.class);
        queryTaskByGroupAndTaskVariant.filter("groupVariant", groupVariant);
        queryTaskByGroupAndTaskVariant.filter("taskVariant", taskVariant);
        queryTaskByGroupAndTaskVariant.filter("isMarked", false);
        return datastore.findAndModify(queryTaskByGroupAndTaskVariant, markTaskOperation);
    }

}

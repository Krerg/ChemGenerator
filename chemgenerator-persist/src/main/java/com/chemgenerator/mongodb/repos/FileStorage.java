package com.chemgenerator.mongodb.repos;

import java.io.File;

/**
 * @author amylnikov
 */
public interface FileStorage {

    void save(byte[] fileBinaryData, String fileName);

    byte[] get(String fileName);

}

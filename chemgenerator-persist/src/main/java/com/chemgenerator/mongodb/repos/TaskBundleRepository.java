package com.chemgenerator.mongodb.repos;

import com.chemgenerator.mongodb.enitites.TaskBundle;
import com.chemgenerator.mongodb.enitites.UserTaskBundles;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

/**
 * @author amylnikov
 */
@Repository
public class TaskBundleRepository extends MongoRepository<UserTaskBundles, ObjectId> {

    public TaskBundleRepository() {
        super(UserTaskBundles.class);
    }
}

package com.chemgenerator.mongodb.repos;

import com.chemgenerator.mongodb.enitites.ProvideTasksMessage;
import com.chemgenerator.mongodb.queue.impl.ProvideTaskQueue;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

@Repository
public class UserOrderRepository extends MongoRepository<ProvideTasksMessage, ObjectId> {

    public UserOrderRepository() {
        super(ProvideTasksMessage.class);
    }

}

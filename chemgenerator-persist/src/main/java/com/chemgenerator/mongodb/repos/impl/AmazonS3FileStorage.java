package com.chemgenerator.mongodb.repos.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.util.Base64;
import com.amazonaws.util.IOUtils;
import com.chemgenerator.mongodb.repos.FileStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.FileCopyUtils;

import javax.annotation.PostConstruct;
import java.io.*;

/**
 * @author amylnikov
 */
@Component
public class AmazonS3FileStorage implements FileStorage {

    private static final Logger LOGGER = LoggerFactory.getLogger(AmazonS3FileStorage.class);

    @Value("${amazon.s3.bucketName}")
    private String bucketName;

    @Autowired
    private AmazonS3 amazonS3Client;

    @PostConstruct
    public void init() {
        if(!amazonS3Client.doesBucketExist(bucketName)) {
            LOGGER.error("Bucket name is not available."
                    + " Try again with a different Bucket name.");
            throw new RuntimeException();
        }
        LOGGER.info("Amazon S3 storage initialized successfully");
    }

    @Override
    public void save(byte[] fileBinaryData, String fileName) {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(fileBinaryData.length);
        String md5Content = new String(Base64.encode(DigestUtils.md5Digest(fileBinaryData)));
        metadata.setContentMD5(md5Content);
        amazonS3Client.putObject(new PutObjectRequest(bucketName,fileName,
                new ByteArrayInputStream(fileBinaryData),metadata));
    }

    @Override
    public byte[] get(String fileName) {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] bytes = IOUtils.toByteArray(amazonS3Client.getObject(bucketName, fileName).getObjectContent());
            bos.close();
            return bytes;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

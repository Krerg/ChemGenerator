package com.chemgenerator.mongodb.config;

import com.chemgenerator.cv.Profiles;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;


/**
 * Mongo configuration for production environment.
 * @author amylnikov
 */
@Configuration
@Profile(value = {Profiles.TEST})
public class MongoTestConfiguration extends MongoConfiguration{

    @Override
    Mongo getMongo() {
        MongoClientURI dbUri = new MongoClientURI("mongodb://chemgen-admin:GkP7bnqllXNSblqM@cluster0-shard-00-00-yfdq7.mongodb.net:27017,cluster0-shard-00-01-yfdq7.mongodb.net:27017,cluster0-shard-00-02-yfdq7.mongodb.net:27017/ChemGenerator?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin");
        return new MongoClient(dbUri);
    }
}

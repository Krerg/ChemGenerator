package com.chemgenerator.mongodb.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 */
@Configuration
public class S3Config {

    @Value("${amazon.s3.accessKey}")
    private String amazonAccessKey;

    @Value("${amazon.s3.secretKey}")
    private String amazonSecretKey;

    @Bean
    public AmazonS3 getAmazonClient() {

        AWSCredentials credentials = new BasicAWSCredentials(
                amazonAccessKey,
                amazonSecretKey
        );

        return AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.US_EAST_2)
                .build();
    }

}

package com.chemgenerator.mongodb.config;

import com.chemgenerator.mongodb.enitites.*;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.springframework.context.annotation.Bean;

import java.net.UnknownHostException;

/**
 * Common Mongo configuration
 * @author amylnikov
 */
public abstract class MongoConfiguration {

    static final String MONGO_DATABSE_NAME = "ChemGenerator";

    abstract Mongo getMongo();

    @Bean
    public Datastore getDB() throws UnknownHostException {
        Morphia morphia = new Morphia();
        morphia.map(User.class);
        morphia.map(TaskEntity.class);
        morphia.map(TaskBundle.class);
        morphia.map(ProvideTasksMessage.class);
        morphia.map(UserTaskBundles.class);
        return morphia.createDatastore((MongoClient) getMongo(), MONGO_DATABSE_NAME);
    }

}

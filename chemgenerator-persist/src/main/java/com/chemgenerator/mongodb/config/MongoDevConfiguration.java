package com.chemgenerator.mongodb.config;

import com.chemgenerator.cv.Profiles;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Mongo configuration for development environment,
 * @author amylnikov
 */
@Configuration
@Profile(value = {Profiles.DEV})
public class MongoDevConfiguration extends MongoConfiguration {

    private static final String MONGO_HOST_NAME = "localhost";

    private static final int MONGO_PORT = 27017;

    @Override
    Mongo getMongo() {
        return new MongoClient(new ServerAddress(MONGO_HOST_NAME, MONGO_PORT));
    }

}

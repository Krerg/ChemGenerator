package com.chemgenerator.mongodb.queue;

/**
 * @author amylnikov
 */
public enum MessageStatus {

    READY, PROCESSING, COMPLETED

}

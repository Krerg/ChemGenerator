package com.chemgenerator.mongodb.queue.impl;

import com.chemgenerator.mongodb.queue.Message;
import com.chemgenerator.mongodb.queue.MessageStatus;
import com.chemgenerator.mongodb.queue.MongoQueue;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

/**
 * @author amylnikov
 * @param <T>
 */
public class MongoQueueImpl<T extends Message> implements MongoQueue<T> {

    private final Class<T> MESSAGE_CLASS;

    @Autowired
    private Datastore ds;

    private Query<T> query;

    private Query<T> queryAll;

    private UpdateOperations<T> resetOperation;

    private UpdateOperations updateOperations;

    public MongoQueueImpl(Class<T> messageClass) {
        this.MESSAGE_CLASS = messageClass;
    }

    @PostConstruct
    public void init() {
        query = ds.createQuery(MESSAGE_CLASS);
        query.filter("messageStatus", MessageStatus.READY);
        updateOperations = ds.createUpdateOperations(MESSAGE_CLASS);
        updateOperations.set("messageStatus",MessageStatus.PROCESSING);
        queryAll = ds.createQuery(MESSAGE_CLASS);
        queryAll.filter("messageStatus",MessageStatus.PROCESSING);
        resetOperation = ds.createUpdateOperations(MESSAGE_CLASS);
        resetOperation.set("messageStatus",MessageStatus.READY);
        resetAllMessages();
    }

    private void resetAllMessages() {
        ds.update(queryAll, resetOperation);
    }

    @Override
    public T get() {
        return (T) ds.findAndModify( query,updateOperations);
    }

    @Override
    public void send(T obj) {
        obj.setMessageStatus(MessageStatus.READY);
        ds.save(obj);
    }

    @Override
    public void ack(T obj) {
        ds.delete(obj);
    }

}

package com.chemgenerator.mongodb.queue;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * @author amylikov
 */
public interface MongoQueue<T extends Message> {

    T get();

    void send(T obj);

    void ack(T obj);

}

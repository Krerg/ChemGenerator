package com.chemgenerator.mongodb.queue;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;
import org.mongodb.morphia.utils.IndexType;

import static org.mongodb.morphia.utils.IndexType.ASC;

/**
 * @author amylnikov
 * @param <T>
 */
@Entity
@Indexes({@Index(fields = @Field(value = "queueName",type = ASC))})
public abstract class Message {

    public Message() {
        this.messageStatus = MessageStatus.READY;
    }

    @Id
    private ObjectId objectId;

    private String queueName;

    private MessageStatus messageStatus;

    public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    public MessageStatus getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(MessageStatus messageStatus) {
        this.messageStatus = messageStatus;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }
}

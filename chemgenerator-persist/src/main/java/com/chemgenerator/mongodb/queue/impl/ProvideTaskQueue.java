package com.chemgenerator.mongodb.queue.impl;

import com.chemgenerator.mongodb.enitites.ProvideTasksMessage;
import org.springframework.stereotype.Component;

/**
 * @author amylnikov
 */
@Component
public class ProvideTaskQueue extends MongoQueueImpl<ProvideTasksMessage> {

    public ProvideTaskQueue() {
        super(ProvideTasksMessage.class);
    }
}

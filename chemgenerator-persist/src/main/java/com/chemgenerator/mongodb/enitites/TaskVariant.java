package com.chemgenerator.mongodb.enitites;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Id;

import java.util.LinkedList;
import java.util.List;

public class TaskVariant {

    @Id
    private ObjectId objectId;

    private String variantName;

    @Embedded
    private List<TaskEntity> tasks;

    public String getVariantName() {
        return variantName;
    }

    public void setVariantName(String variantName) {
        this.variantName = variantName;
    }

    public List<TaskEntity> getTasks() {
        if(tasks == null) {
            tasks = new LinkedList<>();
        }
        return tasks;
    }

    public void setTasks(List<TaskEntity> tasks) {
        this.tasks = tasks;
    }
}

package com.chemgenerator.mongodb.enitites;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Id;
import sun.security.util.ObjectIdentifier;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * @author amylnikov
 */
public class AvailableTaskEntity {

    @Id
    private String groupVariant;

    private List<String> taskVariants;

    public String getGroupVariant() {
        return groupVariant;
    }

    public void setGroupVariant(String groupVariant) {
        this.groupVariant = groupVariant;
    }

    public List<String> getTaskVariants() {
        return taskVariants;
    }

    public void setTaskVariants(List<String> taskVariants) {
        this.taskVariants = taskVariants;
    }
}

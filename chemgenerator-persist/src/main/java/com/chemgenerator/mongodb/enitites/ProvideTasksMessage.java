package com.chemgenerator.mongodb.enitites;

import com.chemgenerator.model.collections.Pair;
import com.chemgenerator.mongodb.queue.Message;
import com.chemgenerator.rest.pojo.request.ProvideTaskPojo;
import org.mongodb.morphia.annotations.Indexed;

import java.util.List;

/**
 * Message for {@link com.chemgenerator.mongodb.queue.impl.ProvideTaskQueue} One
 * message stored information about user's order (provide task to him)
 *
 * @author amylnikov
 */
public class ProvideTasksMessage extends Message {

    public static final String QUEUE_NAME = "provideMessageQueue";

    public ProvideTasksMessage() {
    }

    public ProvideTasksMessage(List<ProvideTaskPojo> taskQueryParameters,
            int amount,
            String bundleName,
            String userName) {
        this.amount = amount;
        this.bundleName = bundleName;
        this.taskQueryParameters = taskQueryParameters;
        this.userLogin = userName;
    }

    /**
     * List of pairs {@link TaskEntity#getTaskVariant()} and
     * {@link TaskEntity#getGroupVariant()}}
     */
    private List<ProvideTaskPojo> taskQueryParameters;

    /**
     * Amount of copies with unique variants
     */
    private int amount;

    private String bundleName;

    /**
     * User's (to whom we will provide tasks) login.
     *
     * @see User#getLogin()
     */
    @Indexed
    private String userLogin;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getBundleName() {
        return bundleName;
    }

    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    public List<ProvideTaskPojo> getTaskQueryParameters() {
        return taskQueryParameters;
    }

    public void setTaskQueryParameters(List<ProvideTaskPojo> taskQueryParameters) {
        this.taskQueryParameters = taskQueryParameters;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }
}

package com.chemgenerator.mongodb.enitites;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Id;

import java.util.List;

/**
 * @author amylnikov
 */
public class TaskEntity {

    @Id
    private ObjectId objectId;

    private String groupVariant;

    private String taskVariant;

    /**
     * Content of task (HTML already)
     */
    private String condition;

    private List<List<String>> taskTable;

    private boolean isMarked;

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    public List<List<String>> getTaskTable() {
        return taskTable;
    }

    public void setTaskTable(List<List<String>> taskTable) {
        this.taskTable = taskTable;
    }

    public String getGroupVariant() {
        return groupVariant;
    }

    public void setGroupVariant(String groupVariant) {
        this.groupVariant = groupVariant;
    }

    public String getTaskVariant() {
        return taskVariant;
    }

    public void setTaskVariant(String taskVariant) {
        this.taskVariant = taskVariant;
    }

    public boolean isMarked() {
        return isMarked;
    }

    public void setMarked(boolean marked) {
        isMarked = marked;
    }
}

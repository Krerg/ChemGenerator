package com.chemgenerator.mongodb.enitites;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

import java.util.LinkedList;
import java.util.List;

/**
 * @author amylniko
 */
public class UserTaskBundles {

    @Id
    private ObjectId objectId;

    @Reference(lazy = true)
    private User user;

    @Embedded
    private List<TaskBundle> taskBundles;

    public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<TaskBundle> getTaskBundles() {
        if(taskBundles==null) {
            taskBundles = new LinkedList<>();
        }
        return taskBundles;
    }

    public void setTaskBundles(List<TaskBundle> taskBundles) {
        this.taskBundles = taskBundles;
    }
}

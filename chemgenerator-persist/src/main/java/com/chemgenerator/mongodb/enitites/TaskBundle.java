package com.chemgenerator.mongodb.enitites;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

import java.util.List;

/**
 * @author amylnikov
 */
public class TaskBundle {

    @Id
    private ObjectId objectId;

    private String bundleName;

    private String bundleFileNameWithAnswers;

    private String bundleFileNameWithoutAnswers;

    @Embedded
    private List<TaskVariant> variants;

    public List<TaskVariant> getTasks() {
        return variants;
    }

    public void setTasks(List<TaskVariant> tasks) {
        this.variants = tasks;
    }

    public String getBundleName() {
        return bundleName;
    }

    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    public String getBundleFileNameWithAnswers() {
        return bundleFileNameWithAnswers;
    }

    public void setBundleFileNameWithAnswers(String bundleFileNameWithAnswers) {
        this.bundleFileNameWithAnswers = bundleFileNameWithAnswers;
    }

    public String getBundleFileNameWithoutAnswers() {
        return bundleFileNameWithoutAnswers;
    }

    public void setBundleFileNameWithoutAnswers(String bundleFileNameWithoutAnswers) {
        this.bundleFileNameWithoutAnswers = bundleFileNameWithoutAnswers;
    }
}

package com.chemgenerator.mongodb.enitites;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;

import java.util.Date;

public class User {

    @Id
    private ObjectId id;

    @Indexed(unique = true)
    private String login;

    private String name;

    private String password;

    private String phoneNumber;

    private String email;

    private Date birth;

    private String imagePath;

    @Reference(lazy = true)
    private UserTaskBundles userBundles;

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User() {}

    public User(String userName, String password, String phoneNumber) {
        this.name = userName;
        this.password = password;
		this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

    public ObjectId getId() {
        return id;
    }

    public UserTaskBundles getUserBundles() {
        return userBundles;
    }

    public void setUserBundles(UserTaskBundles userBundles) {
        this.userBundles = userBundles;
    }

    @Override
	public String toString() {
		return "User{" +
				"login='" + login + '\'' +
				", name='" + name + '\'' +
				", phoneNumber='" + phoneNumber + '\'' +
				", email='" + email + '\'' +
				", birth=" + birth +
				", imagePath=" + imagePath +
				'}';
	}


}

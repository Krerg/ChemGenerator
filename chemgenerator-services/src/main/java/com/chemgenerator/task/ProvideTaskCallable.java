package com.chemgenerator.task;

import com.chemgenerator.exception.NoTasksToProvideException;
import com.chemgenerator.mongodb.enitites.ProvideTasksMessage;
import com.chemgenerator.mongodb.queue.impl.ProvideTaskQueue;
import com.chemgenerator.service.impl.TaskServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.Callable;

/**
 * @author amylnikov
 */
@Component
@Scope(value = "prototype")
public class ProvideTaskCallable implements Callable<Void> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProvideTaskCallable.class);

    private static boolean isStopped = false;

    @Autowired
    private TaskServiceImpl taskService;

    @Autowired
    private ProvideTaskQueue provideTaskQueue;

    @Override
    public Void call() throws Exception {
        while (!isStopped) {
            ProvideTasksMessage message = provideTaskQueue.get();
            if (message != null) {
                if (processMessage(message)) {
                    provideTaskQueue.ack(message);
                } else {
                    provideTaskQueue.ack(message);
                    message.setObjectId(null);
                    provideTaskQueue.send(message);
                }
            }
        }
        return null;
    }

    private boolean processMessage(ProvideTasksMessage message) {
        try {
            taskService.provideTasksToUser(message.getUserLogin(),
                    message.getTaskQueryParameters(),
                    message.getAmount(),
                    message.getBundleName());
            return true;
        } catch (NoTasksToProvideException ntpe) {
            LOGGER.error("There is no tasks for next parameters: "+message.getTaskQueryParameters());
            //TODO notify to email by it
            return false;
        } catch (Exception e) {
            LOGGER.error("Error while processing message " + message.getBundleName()
                    + " for user " + message.getUserLogin());
            return false;
        }
    }
}

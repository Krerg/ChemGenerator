package com.chemgenerator.task;

/**
 * @author amylniko
 */
public interface TaskRelatedValuesStorage {

    String getRelatedValue(String taskVariant, String groupVariant);

    String getTaskIdFromName(String taskName);

}

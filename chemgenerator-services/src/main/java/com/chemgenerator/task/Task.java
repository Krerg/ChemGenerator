package com.chemgenerator.task;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author amylnikov
 */
public class Task {

    private int taskNumber;

    private int groupVariant;

    private int taskVariant;

    private String taskName;

    private String condition="";

    private String title;

    private Table table;

    public class Table {

        public Map<Integer, Map<Integer, StringBuilder>> getTableContent() {
            return tableContent;
        }

        private Map<Integer,Map<Integer,StringBuilder>> tableContent;

        private Table() {
            tableContent = new LinkedHashMap<>();
        }

    }

    public int getGroupVariant() {
        return groupVariant;
    }

    public void setGroupVariant(int groupVariant) {
        this.groupVariant = groupVariant;
    }

    public int getTaskVariant() {
        return taskVariant;
    }

    public void setTaskVariant(int taskVariant) {
        this.taskVariant = taskVariant;
    }

    public int getTaskNumber() {
        return taskNumber;
    }

    public void setTaskNumber(int taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public void addCharacterToTable(String c, int row, int column) {
        if(table == null) {
            this.table = new Table();
        }

        Map<Integer,StringBuilder> tableRow;
        if(table.tableContent.get(row)==null) {
            tableRow = new LinkedHashMap<>();
            table.tableContent.put(row,tableRow);
        } else {
            tableRow = table.tableContent.get(row);
        }

        if(tableRow.get(column)!=null) {
            tableRow.get(column).append(c);
        } else {
            tableRow.put(column,new StringBuilder(c));
        }

    }

    public void addCharacterToTitle(String c, boolean upCh, boolean subCh) {
        if(upCh && subCh) {
            //TODO throw customException
        }
        title+=c;
    }

    public void addCharacterToCondition(String c, boolean upCh, boolean subCh) {
        if(upCh && subCh) {
            //TODO throw customException
        }
        condition+=c;
    }

    public Table getTable() {
        return table;
    }
}

package com.chemgenerator.task.impl;

import com.chemgenerator.task.TaskRelatedValuesStorage;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.util.StringUtils.isEmpty;

/**
 * @author amylniko
 */
@Component(value = "defaultTaskRelatedValuesStorage")
public class DefaultTaskRelatedValuesStorage implements TaskRelatedValuesStorage {

    private Map<String,String> relatedValues = new HashMap(150);

    private Map<String,String> invertedRelatedValues = new HashMap<>(150);

    @PostConstruct
    void initRelatedValues() {
        relatedValues.put("1","Периодическая система и структура атомов");
        relatedValues.put("1_1","Определение характеристик элемента с известным названием (1-4 периоды)");
        relatedValues.put("1_2","Определение характеристик элемента с данным названием (5-7 периоды)");
        relatedValues.put("1_3","Определение характеристик элемента с данным обозначением (1-4 периоды)");
        relatedValues.put("1_4","Определение характеристик элемента с данным обозначением (5-7 периоды)");
        relatedValues.put("1_5","Определение характеристик элемента с данной относительной атомной массой (1-4 периоды)");
        relatedValues.put("1_6","Определение характеристик элемента с данной относительной атомной массой (5-7 периоды)");
        relatedValues.put("1_7","Определение характеристик элемента  (число электронов, протонов, нейтронов, нергетических уровней; 1-4 периоды");
        relatedValues.put("1_8","Определение характеристик элемента  (число электронов, протонов, нейтронов, нергетических уровней; 5-7 периоды");
        relatedValues.put("1_9","Составление электронной конфигурации по характеристикам элемента  (1-4 периоды)");
        relatedValues.put("1_10","Составление электронной конфигурации по характеристикам элемента  (5-7 периоды)");

        relatedValues.put("2","Относительная молекулярная масса, молярная масса, закон Авогадро, базовые количественные расчёты");
        relatedValues.put("2_1","(А) Составление названий соединений по формуле и расчёт их молярной и относительной молекулярной массы");
        relatedValues.put("2_2","(Б) Составление названий соединений по формуле и расчёт их молярной и относительной молекулярной массы");
        relatedValues.put("2_3","Вычисление массовой доли элементов в соединении");
        relatedValues.put("2_4","Определение оксида с большей массовой долей кислорода");
        relatedValues.put("2_5","Расчёт массы элемента, которую можно выделить из соединения");
        relatedValues.put("2_6","Расчёт  массы соединения, необходимой для получения заданной массы простого вещества");
        relatedValues.put("2_7","Расчёт количества вещества по его массе");
        relatedValues.put("2_8","Расёт массы вещества по его количеству");
        relatedValues.put("2_9","Расчёт числа частиц в веществе по его массе");
        relatedValues.put("2_10","Расчёт массы, массовой доли, количества и числа атомов в образце вещества");
        relatedValues.put("2_11","Определение формулы и названия вещества по массовой доле составляющих элементов");
        relatedValues.put("2_12","Определение формулы и названия вещества по массе составляющих элементов");

        relatedValues.put("3","Молярный объём газов,  базовые рассчёты при нормальных условиях");
        relatedValues.put("3_1","Определение объёма газообразного простого вещества при нормальных условиях по его массе");
        relatedValues.put("3_2","Определение массы газообразного простого вещества при нормальных условиях по его объёму");
        relatedValues.put("3_3","Определение простого газообразного вещества при н.у., если известны его объём и масса");
        relatedValues.put("3_4","Определение объёма смеси двух газов известного состава при нормальных условиях");
        relatedValues.put("3_5","Определение массы каждого из двух газов содержащихся  в газовой смеси известного объёма и массы при нормальных условиях.");
        relatedValues.put("3_6","Определение массовых долей простых веществ в смеси двух газов при известном объёме смеси (н.у.)");
        relatedValues.put("3_7","Определение числа молекул газа по известному объёму (н.у.)");
        relatedValues.put("3_8","Определение объёма газа (н.у.) по известному числу молекул");

        relatedValues.put("4","Молярный объём газов, уравнение Клапейрона-Менделеева");
        relatedValues.put("4_1","Определение объёма простого газообразного вещества по закону Клапейрона-Менделеева");
        relatedValues.put("4_2","Определение давления в сосуде с простым газообразным веществом по закону Клапейрона-Менделеева");
        relatedValues.put("4_3","Определение температуры в сосуде с простым газообразным веществом по закону Клапейрона-Менделеева");
        relatedValues.put("4_4","Определение массы  простого газообразного вещества по закону Клапейрона-Менделеева");
        relatedValues.put("4_5","Определение простого газообразного вещества известной массы и объёма при данных условиях (p,t(°C))");
        relatedValues.put("4_6","Определение t(oC)  смеси двух газов для известных p,V, m1 и m2");
        relatedValues.put("4_7","Определение давление смеси двух газов для известных t(°C),V, m1 и m2 ");
        relatedValues.put("4_8","Определение давления смеси двух газов для известных t(°C),V1,V2, m. (усложнённо) ");
        relatedValues.put("4_9","Определение t(°C) смеси двух газов для известных p,V1,V2, m. (усложнённо)");
        relatedValues.put("4_10","Определение массовых долей простых веществ в газовой смеси для известных m,p,V, t(°C)");

        relatedValues.put("5","Простые рассчёты для реакций в газовой фазе");
        relatedValues.put("5_1","Определение давления газовой смеси после реакции (известны массы реагентов)");
        relatedValues.put("5_2","Определение температуры газовой смеси после реакции  (известны массы реагентов)");
        relatedValues.put("5_3","Определение давления газовой смеси после реакции при данных параметрах, массе и объёме");
        relatedValues.put("5_4","Определение температуры газовой смеси после реакции (известны начальные m,p,V, t(oC) смеси).");
        relatedValues.put("5_5","Определение состава газовой смеси  (известны p,V, t(oC)   смеси до и после реакции ) (усложнённо)");
        relatedValues.put("5_6","Определение давления в сосуде при выпаривании раствора");
        relatedValues.put("5_7","Определение температуры в сосуде при выпаривании раствора");
        relatedValues.put("5_8","Определение концентрации раствора по данным о температуре и давлении в сосуде после выпаривания раствора");

        relatedValues.put("6","Оксиды, гидроксиды, их классификация и свойства, соли");
        relatedValues.put("6_1","Запись формулы оксида по его названию (3 оксида); 1-4 периоды");
        relatedValues.put("6_2","Запись формулы оксида по его названию (2 оксида); 5-7 периоды");
        relatedValues.put("6_3","Запись названия оксида по его формуле (3 оксида); 1-4 периоды");
        relatedValues.put("6_4","Запись названия оксида по его формуле (2 оксида); 5-7 периоды");
        relatedValues.put("6_5","Оксиды, гидроксиды,  их классификация и свойства (основные и кислотные)");
        relatedValues.put("6_6","Доказательство химических свойств основных оксидов и гидроксидов");
        relatedValues.put("6_7","Доказательство химических свойств кислотных оксидов и гидроксидов");
        relatedValues.put("6_8","Напишите уравнение химической реакции образования оксида по названию (2 оксида); 1-4 периоды");
        relatedValues.put("6_9","Составление уравнений химических реакций образования основного гидроксида (щёлочи) по названию (А)");
        relatedValues.put("6_10","Составление уравнений реакций образования основного гидроксида зная название (Б)");
        relatedValues.put("6_11","Составление уравнений реакций взаимодействия щелочей с кислотными оксидами с образованием средних и кислых солей");
        relatedValues.put("6_12","Составление уравнений диссоциации веществ");

        relatedValues.put("7","Характерные химические реакции амфотерных оксидов и гидроксидов");
        relatedValues.put("7_1","Составление уравнений реакций получения амфотерных гидроксидов и их взаимодействий в растворе");
        relatedValues.put("7_2","Составление уравнений реакций амфотерных оксидов и гидроксидов со щелочами при сплавлении");
        relatedValues.put("7_3","Термическое разложение и гидролиз солей амфотерных гидроксидов");
        relatedValues.put("7_4","Составление реакций взаимодействия солей амфотерных гидроксидов с кислотами");

        relatedValues.put("8","Классификация веществ и номенклатура");
        relatedValues.put("8_1","Составление названия соединения по его формуле и расчёт относительной молекулярной массы ");
        relatedValues.put("8_2","Составление формулы соединения по его названию  и расчёт относительной молекулярной массы ");
        relatedValues.put("8_3","Запись названия соединения по его формуле (включая сложные соединения)");
        relatedValues.put("8_4","Запись формулы соединения по его названию (включая сложные соединения)");
        relatedValues.put("8_5","Определение класса соединений (кислотный или основный оксид, соль, кислота и т.д.)  ");
        relatedValues.put("8_6","Запись названия соединения по его формуле (факультативно)");
        relatedValues.put("8_7","Запись формулы соединения по его названию (факультативно)");
        relatedValues.put("8_8","Составление формулы вещества по названию и определение в ней степени окисления элементов");
        relatedValues.put("8_9","Составление названия вещества по формуле и определение в ней степени окисления элементов ");

        relatedValues.put("9","Базовые химические реакции");
        relatedValues.put("9_1","Составление уравнений реакций соединения");
        relatedValues.put("9_2","Составление уравнений реакций разложения");
        relatedValues.put("9_3","Составление уравнений реакций замещения");
        relatedValues.put("9_4","Составление уравнений реакций обмена");
        relatedValues.put("9_5","Образование и нейтрализация кислых солей ");

        relatedValues.put("10","Определение коэффициентов в уравнениях реакций соединения, разложения, замещения и обмена");
        relatedValues.put("10_1","Определение коэффициентов в уравнениях реакций соединения");
        relatedValues.put("10_2","Определение коэффициентов в уравнениях реакций разложения");
        relatedValues.put("10_3","Определение коэффициентов в уравнениях реакций замещения");
        relatedValues.put("10_4","Определение коэффициентов в уравнениях реакций обмена");
        relatedValues.put("10_5","Определение коэффициентов в окислительно-восстановительных реакциях (усложненные варианты)");

        relatedValues.put("11","Вычисление массы реагентов и продуктов реакций по массе одного из реагентов");
        relatedValues.put("11_1","Вычисление массы реагента и продукта в реакциях соединения по массе одного из реагентов");
        relatedValues.put("11_2","Вычисление массы продуктов в реакции разложения по массе реагента");
        relatedValues.put("11_3","Вычисление массы реагентов и продуктов в реакции замещения по массе одного из реагентов");
        relatedValues.put("11_4","Вычисление массы реагентов и продуктов в реакции обмена по массе одного из реагентов");

        relatedValues.put("12","Нахождение массы  реагентов и продуктов реакции по массе одного из продуктов");
        relatedValues.put("12_1","Вычисление массы  реагентов и продуктов в реакции соединения по массе одного из продуктов");
        relatedValues.put("12_2","Вычисление массы  реагентов и продуктов в реакции разложения по массе одного из продуктов");
        relatedValues.put("12_3","Вычисление массы реагентов и продуктов в реакции замещения по массе одного из продуктов");
        relatedValues.put("12_4","Вычисление массы реагентов и продуктов в реакции обмена по массе одного из продуктов");

        relatedValues.put("13","Важнейшие индивидуальные химические свойства веществ");
        relatedValues.put("13_1","Основные химические свойства водорода");
        relatedValues.put("13_2","Основные химические свойства кислорода ");
        relatedValues.put("13_3","Основные химические свойства щелочных металлов  и их соединений");
        relatedValues.put("13_4","Основные химические свойства  галогенов");
        relatedValues.put("13_5","Основные химические свойства  углерода");
        relatedValues.put("13_6","Основные химические свойства соединений, содержащих азот");

        relatedValues.put("14","Расчёты содержания веществ в растворах");
        relatedValues.put("14_1","Расчёт плотности раствора по массе и объёму");
        relatedValues.put("14_2","Расчёт массы раствора по объёму и плотности");
        relatedValues.put("14_3","Расчёт объёма раствора по массе и плотности");
        relatedValues.put("14_4","Расчёт массы веществ, составляющих раствор по известной массе и концентрации раствора");
        relatedValues.put("14_5","Расчёт массы веществ, составляющих раствор по известным: объёму, концентрации и плотности раствора");
        relatedValues.put("14_6","Расчёт массы твёрдого вещества необходимой для получения раствора требуемой концентрации");
        relatedValues.put("14_7","Расчёт объёма воды, которую надо добавить к первоначальному раствору для получения раствора требуемой концентрации");
        relatedValues.put("14_8","Расчёт объёма раствора, который надо добавить к воде, для получения раствора требуемой концентрации");
        relatedValues.put("14_9","Расчёт массы твёрдого вещества, которую необходимо добавить к существующему раствору для получения раствора требуемой концентрации");
        relatedValues.put("14_10","Определение концентрации раствора, полученного смешением двух растворов с известной концентрацией и массой");
        relatedValues.put("14_11","Определение концентрации раствора, полученного смешением двух растворов с известными объёмом, плотностью и концентрацией");

        relatedValues.put("15","Моли, эквиваленты и простые расчёты");
        relatedValues.put("15_1","Молярные массы простых веществ (для химических элементов 1-4 периодов)");
        relatedValues.put("15_2","Молярные массы и массы эквивалента соединений щелочных металлов (валентность I)");
        relatedValues.put("15_3","Молярные массы и массы эквивалента соединений щелочноземельных металлов (валентность (II))");
        relatedValues.put("15_4","Молярные массы и массы эквивалентов галогенов и галогеноводородов (основность (I))");
        relatedValues.put("15_5","Молярные массы простых веществ, оксидов и массы эквивалентов (для химических элементов 1-4 периодов)");
        relatedValues.put("15_6","Молярная масса и эквивалент кислородосодержащих кислот");
        relatedValues.put("15_7","Молярная масса и масса эквивалентов элементов с переменной валентностью (для элементов 1-4 периодов)");
        relatedValues.put("15_8","Вычисление молярной массы и массы эквивалента (щелочной металл, оксид, гидроксид, соль)");
        relatedValues.put("15_9","Обратная задача - поиск элемента по эквиваленту  с точностью 0,0001");
        relatedValues.put("15_10","Обратная задача - поиск элемента по эквиваленту  с точностью 0,001 - (усложнённо)");
        relatedValues.put("15_11","Обратная задача - поиск элемента по эквиваленту  с точностью 0,003 - (усложнённо)");

        relatedValues.put("16","Расчётные задачи с использованием молярных масс и эквивалентов");
        relatedValues.put("16_1","Расчёт массы и объёма кислорода и оксида в реакции окисления");
        relatedValues.put("16_2","Расчёт массы основания в реакции нейтрализации");
        relatedValues.put("16_3","Определение металла в реакции восстановления (с хитринкой) с точностью 0,0001");
        relatedValues.put("16_4","Определение металла в реакции восстановления (с хитринкой) с точностью 0,001 (усложнённо)");
        relatedValues.put("16_5","Определение металла в реакции восстановления (с хитринкой) с точностью 0,003 (усложнённо)");
        relatedValues.put("16_6","Расчёт состава газовой смеси (усложнённо)");
        relatedValues.put("16_7","Определение металла в сложном анализе");

        relatedValues.put("17","Примеры окислительно-восстановительных реакций");
        relatedValues.put("17_1","Реакции MeMnO4 в зависимости от среды");
        relatedValues.put("17_2","Реакция металлов с азотной кислотой в зависимости от концентрации и металла");
        relatedValues.put("17_3","Реакция диспропорционирования галогенов со щелочами при нагревании и охлаждении");
        relatedValues.put("17_4","Реакция разложения нитратов");
        relatedValues.put("17_5","Реакции термического разложения солей аммония");
        relatedValues.put("17_6","Реакции термического разложения солей ");

        invertValues();

    }

    private void invertValues() {
        for(String name: relatedValues.keySet()){
            invertedRelatedValues.put(relatedValues.get(name), name);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRelatedValue(String taskVariant, String  groupVariant) {
        if(isEmpty(taskVariant)) {
            return relatedValues.getOrDefault(groupVariant,"");
        }
        return relatedValues.getOrDefault(groupVariant+"_"+taskVariant,"");
    }

    @Override
    public String getTaskIdFromName(String taskName) {
        String taskId = invertedRelatedValues.get(taskName);
        if(!taskId.contains("_")) {
            return taskId;
        }
        taskId = taskId.split("_")[1];
        return taskId;
    }
}

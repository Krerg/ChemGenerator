package com.chemgenerator.task;

import com.chemgenerator.config.ASCIIRus;
import com.chemgenerator.config.impl.ASCIIRusImpl;
import com.chemgenerator.exception.IncorrectFileFormatException;
import com.chemgenerator.utils.BodyPositionCV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

/**
 * @author amylnikov
 */
@Component
public class TasksBuilder {

    private ASCIIRus encodingTable = new ASCIIRusImpl();

    private static Logger LOGGER = LoggerFactory.getLogger(TasksBuilder.class);

    private Task currentTask;

    private List<Task> tasks;

    private int taskNumber;

    private int character;

    private int row;

    private int column;

    private int subCharacter;

    private int upCharacter;

    private int bodyPosition;

    private int groupVariant;

    private int taskVariant;

    private String task;

    private void nextTask() {
        currentTask = new Task();
        tasks.add(currentTask);
    }

    public TasksBuilder() {
        task = "";
        tasks = new LinkedList<>();
        nextTask();
    }

    public void addLine(String line) throws IncorrectFileFormatException {
        int taskNumber;
        String[] splitLine = line.split(";");
        if(splitLine.length!=17) {
            throw new IncorrectFileFormatException();
        }

        try {
            groupVariant = Integer.parseInt(splitLine[4]);
        } catch (NumberFormatException e) {
            LOGGER.error("Exception while parsing task number with given input {"+splitLine[3]+"}");
            throw  new IncorrectFileFormatException();
        }

        try {
            taskVariant = Integer.parseInt(splitLine[5]);
        } catch (NumberFormatException e) {
            LOGGER.error("Exception while parsing task number with given input {"+splitLine[4]+"}");
            throw  new IncorrectFileFormatException();
        }

        try {
            taskNumber = Integer.parseInt(splitLine[3]);
        } catch (NumberFormatException e) {
            LOGGER.error("Exception while parsing task number with given input {"+splitLine[3]+"}");
            throw  new IncorrectFileFormatException();
        }

        try {
            bodyPosition = Integer.parseInt(splitLine[6]);
        } catch (NumberFormatException e) {
            LOGGER.error("Exception while parsing body position with given input {"+splitLine[7]+"}");
            throw  new IncorrectFileFormatException();
        }

        try {
            row = Integer.parseInt(splitLine[7]);
        } catch (NumberFormatException e) {
            LOGGER.error("Exception while parsing row with given input {"+splitLine[7]+"}");
            throw  new IncorrectFileFormatException();
        }

        try {
            character =  Integer.parseInt(splitLine[10]);
        } catch (NumberFormatException e) {
            LOGGER.error("Exception while parsing character with given input {"+splitLine[10]+"}");
            throw  new IncorrectFileFormatException();
        }

        try {
            column = Integer.parseInt(splitLine[8]);
        } catch (NumberFormatException e) {
            LOGGER.error("Exception while parsing column with given input {"+splitLine[8]+"}");
            throw  new IncorrectFileFormatException();
        }

        try {
            subCharacter = Integer.parseInt(splitLine[11]);
        } catch (NumberFormatException e) {
            LOGGER.error("Exception while parsing subcharacter flag with given input {"+splitLine[11]+"}");
            throw  new IncorrectFileFormatException();
        }

        try {
            upCharacter = Integer.parseInt(splitLine[12]);
        } catch (NumberFormatException e) {
            LOGGER.error("Exception while parsing upcharacter flag with given input {"+splitLine[12]+"}");
            throw  new IncorrectFileFormatException();
        }

        if(currentTask.getTaskNumber() == 0) {
            currentTask.setTaskNumber(taskNumber);
        } else {
            if(currentTask.getTaskNumber()!=taskNumber) {
                task="";
                nextTask();
                currentTask.setTaskNumber(taskNumber);
            }
        }

        currentTask.setGroupVariant(groupVariant);
        currentTask.setTaskVariant(taskVariant);

        String c = encodingTable.getLetter(character);
        if(c == null) {
            c = "";
        }

        String formattedCharacter = getFormattedCharacter(c,upCharacter,subCharacter);

        switch (bodyPosition) {
            case BodyPositionCV.TITLE :
                currentTask.addCharacterToTitle(formattedCharacter,false,false);
                return;

            case BodyPositionCV.CONDITION:
                currentTask.addCharacterToCondition(formattedCharacter,false,false);
                return;

            case BodyPositionCV.TABLE:
                currentTask.addCharacterToTable(formattedCharacter, row, column);
                return;
        }

    }

    public List<Task> build() {
        currentTask = null;
        return tasks;
    }

    private String getFormattedCharacter(String c, int upCharacter, int subCharacter) throws IncorrectFileFormatException {
        if(upCharacter == 1 && subCharacter == 1) {
            LOGGER.error("Exception while parsing character because it cannot be up and sub: {"+c+"}");
            //throw new IncorrectFileFormatException();
            return c;
        } else if(subCharacter == 1) {
            return "<sub>"+c+"</sub>";
        } else if(upCharacter == 1) {
            return "<sup>"+c+"</sup>";
        } else {
            return c;
        }
    }

}

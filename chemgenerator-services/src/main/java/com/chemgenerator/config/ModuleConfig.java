package com.chemgenerator.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 *
 * Spring configuration for parser module.
 * Enables spring scheduling
 *
 * @author amylnikov
 */
@Configuration
@EnableScheduling
public class ModuleConfig {
}

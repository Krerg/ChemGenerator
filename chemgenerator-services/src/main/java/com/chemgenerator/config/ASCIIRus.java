package com.chemgenerator.config;

import org.springframework.stereotype.Component;

/**
 * @author amylnikov
 */
public interface ASCIIRus {

    String getLetter(int character);

}

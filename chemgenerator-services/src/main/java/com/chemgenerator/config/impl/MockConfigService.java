package com.chemgenerator.config.impl;

import com.chemgenerator.config.ConfigCV;
import com.chemgenerator.config.ConfigService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Mock implementation for configuration service with hardcoded values
 *
 * @author amylnikov
 */
@Component("mockConfigService")
public class MockConfigService implements ConfigService {

    private static Map<String, Object> configs;

    @PostConstruct
    public void setUpMockData() {
        configs = new HashMap<>();
        configs.put(ConfigCV.LOOKUP_FOLDER_PATH,System.getProperty("user.dir")+"/tasks/");
        configs.put(ConfigCV.THREAD_POOL_THREAD_CAPACITY, 1);
        configs.put(ConfigCV.TASK_BUNDLE_DIRECTORY,System.getProperty("user.dir")+"/taskBundles/");
        initFolders();
    }

    private void initFolders() {
        Path bundleDirectoryPath = Paths.get(String.valueOf(getConfig(ConfigCV.TASK_BUNDLE_DIRECTORY)));
        if(!Files.exists(bundleDirectoryPath)) {
            try {
                Files.createDirectories(bundleDirectoryPath);
            } catch (IOException e) {

            }
        }
    }

    @Override
    public Object getConfig(String configCV) {
        return configs.get(configCV);
    }

    @Override
    public void readConfigs() {

    }

    @Override
    public void clearConfigCache() {
    }
}

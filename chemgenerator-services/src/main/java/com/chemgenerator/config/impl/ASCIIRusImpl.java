package com.chemgenerator.config.impl;

import com.chemgenerator.config.ASCIIRus;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author amylnikov
 */
@Component("ASCIIRusImpl")
public class ASCIIRusImpl implements ASCIIRus {

    private static Map<Integer, String> letterMap;

    public ASCIIRusImpl() {

        letterMap = new HashMap<>(255);

        letterMap.put(0," ");
        letterMap.put(1," ");
        letterMap.put(2," ");
        letterMap.put(3," ");
        letterMap.put(4," ");
        letterMap.put(5," ");
        letterMap.put(7," ");
        letterMap.put(8," ");
        letterMap.put(9," ");
        letterMap.put(10," ");
        letterMap.put(13," ");

        letterMap.put(32," ");
        letterMap.put(33,"!");
        letterMap.put(34,"'");
        letterMap.put(35,"#");
        letterMap.put(36,"$");
        letterMap.put(37,"%");
        letterMap.put(38,"&");
        letterMap.put(39,"'");
        letterMap.put(40,"(");
        letterMap.put(41,")");
        letterMap.put(42,"*");
        letterMap.put(43,"+");
        letterMap.put(44,",");
        letterMap.put(45,"-");
        letterMap.put(46,".");
        letterMap.put(47,"/");
        letterMap.put(48,"0");
        letterMap.put(49,"1");
        letterMap.put(50,"2");
        letterMap.put(51,"3");
        letterMap.put(52,"4");
        letterMap.put(53,"5");
        letterMap.put(54,"6");
        letterMap.put(55,"7");
        letterMap.put(56,"8");
        letterMap.put(57,"9");
        letterMap.put(58,":");
        letterMap.put(59,";");
        letterMap.put(60,"<");
        letterMap.put(61,"=");
        letterMap.put(62,">");
        letterMap.put(63,"?");
        letterMap.put(64,"@");
        letterMap.put(65,"A");
        letterMap.put(66,"B");
        letterMap.put(67,"C");
        letterMap.put(68,"D");
        letterMap.put(69,"E");
        letterMap.put(70,"F");
        letterMap.put(71,"G");
        letterMap.put(72,"H");
        letterMap.put(73,"I");
        letterMap.put(74,"J");
        letterMap.put(75,"K");
        letterMap.put(76,"L");
        letterMap.put(77,"M");
        letterMap.put(78,"N");
        letterMap.put(79,"O");
        letterMap.put(80,"P");
        letterMap.put(81,"Q");
        letterMap.put(82,"R");
        letterMap.put(83,"S");
        letterMap.put(84,"T");
        letterMap.put(85,"U");
        letterMap.put(86,"V");
        letterMap.put(87,"W");
        letterMap.put(88,"X");
        letterMap.put(89,"Y");
        letterMap.put(90,"Z");
        letterMap.put(91,"[");
        letterMap.put(92,"\\");
        letterMap.put(93,"]");
        letterMap.put(94,"^");
        letterMap.put(95,"_");
        letterMap.put(96,"`");
        letterMap.put(97,"a");
        letterMap.put(98,"b");
        letterMap.put(99,"c");
        letterMap.put(100,"d");
        letterMap.put(101,"e");
        letterMap.put(102,"f");
        letterMap.put(103,"g");
        letterMap.put(104,"h");
        letterMap.put(105,"i");
        letterMap.put(106,"j");
        letterMap.put(107,"k");
        letterMap.put(108,"l");
        letterMap.put(109,"m");
        letterMap.put(110,"n");
        letterMap.put(111,"o");
        letterMap.put(112,"p");
        letterMap.put(113,"q");
        letterMap.put(114,"r");
        letterMap.put(115,"s");
        letterMap.put(116,"t");
        letterMap.put(117,"u");
        letterMap.put(118,"v");
        letterMap.put(119,"w");
        letterMap.put(120,"x");
        letterMap.put(121,"y");
        letterMap.put(122,"z");
        letterMap.put(123,"~");
        letterMap.put(124,"DEL");
        letterMap.put(125,"");
        letterMap.put(126,"");
        letterMap.put(127,"");
        letterMap.put(128,"Ђ");
        letterMap.put(129,"Ѓ");
        letterMap.put(130,"‚");
        letterMap.put(131,"ѓ");
        letterMap.put(132,"„");
        letterMap.put(133,"…");
        letterMap.put(134,"†");
        letterMap.put(135,"l");
        letterMap.put(136,"l");
        letterMap.put(137,"l");
        letterMap.put(152,"?");
        letterMap.put(168,"Ё");
        letterMap.put(184,"ё");
        letterMap.put(192,"А");
        letterMap.put(193,"Б");
        letterMap.put(194,"В");
        letterMap.put(195,"Г");
        letterMap.put(196,"Д");
        letterMap.put(197,"Е");
        letterMap.put(198,"Ж");
        letterMap.put(199,"З");
        letterMap.put(200,"И");
        letterMap.put(201,"Й");
        letterMap.put(202,"К");
        letterMap.put(203,"Л");
        letterMap.put(204,"М");
        letterMap.put(205,"Н");
        letterMap.put(206,"О");
        letterMap.put(207,"П");
        letterMap.put(208,"Р");
        letterMap.put(209,"С");
        letterMap.put(210,"Т");
        letterMap.put(211,"У");
        letterMap.put(212,"Ф");
        letterMap.put(213,"Х");
        letterMap.put(214,"Ц");
        letterMap.put(215,"Ч");
        letterMap.put(216,"Ш");
        letterMap.put(217,"Щ");
        letterMap.put(218,"Ъ");
        letterMap.put(219,"Ы");
        letterMap.put(220,"Ь");
        letterMap.put(221,"Э");
        letterMap.put(222,"Ю");
        letterMap.put(223,"Я");
        letterMap.put(224,"а");
        letterMap.put(225,"б");
        letterMap.put(226,"в");
        letterMap.put(227,"г");
        letterMap.put(228,"д");
        letterMap.put(229,"е");
        letterMap.put(230,"ж");
        letterMap.put(231,"з");
        letterMap.put(232,"и");
        letterMap.put(233,"й");
        letterMap.put(234,"к");
        letterMap.put(235,"л");
        letterMap.put(236,"м");
        letterMap.put(237,"н");
        letterMap.put(238,"о");
        letterMap.put(239,"п");
        letterMap.put(240,"р");
        letterMap.put(241,"с");
        letterMap.put(242,"т");
        letterMap.put(243,"у");
        letterMap.put(244,"ф");
        letterMap.put(245,"х");
        letterMap.put(246,"ц");
        letterMap.put(247,"ч");
        letterMap.put(248,"ш");
        letterMap.put(249,"щ");
        letterMap.put(250,"ъ");
        letterMap.put(251,"ы");
        letterMap.put(252,"ь");
        letterMap.put(253,"э");
        letterMap.put(254,"ю");
        letterMap.put(255,"я");

    }

    @Override
    public String getLetter(int character) {
        if(letterMap.get(character)==null) {
            System.out.println("warn:"+character);
        }
        return letterMap.get(character);
    }
}

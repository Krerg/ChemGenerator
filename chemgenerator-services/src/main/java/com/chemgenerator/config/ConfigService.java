package com.chemgenerator.config;

/**
 * Interface for configuration service which provides value from DB by key.
 *
 * @author amylnikov
 */
public interface ConfigService {

    /**
     * Gets config value by given constant.
     *
     * @param configCV constant value of config {@link ConfigCV}
     * @return null if config not exists, otherwise config value
     */
    Object getConfig(String configCV);

    /**
     * Reads all configs from database and caches them (if it supported by implementation)
     */
    void readConfigs();

    /**
     * Clears config cache.
     *
     * NOTE! Implementation of config service could not have
     * the config cache
     *
     */
    void clearConfigCache();

}

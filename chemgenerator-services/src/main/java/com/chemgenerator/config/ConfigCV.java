package com.chemgenerator.config;

import com.chemgenerator.service.ParserService;

/**
 * Constant values for configs.
 *
 * @see ConfigService
 *
 * @author amylnikov
 */
public class ConfigCV {

    /**
     * Config CV for lookup folder {@link ParserService}
     */
    public static final String LOOKUP_FOLDER_PATH = "lookupFolder";

    /**
     * Number of working threads for executor service {@link com.chemgenerator.service.impl.TaskServiceImpl#executorService}
     */
    public static final String THREAD_POOL_THREAD_CAPACITY = "threadPoolCapacity";

    /**
     * Directory where stored user's task bundles.
     */
    public static final String TASK_BUNDLE_DIRECTORY = "taskBundles";

    /**
     *  Available spring profiles.
     */
    enum PROFILES {
        DEV("dev"), PROD("prod");

        PROFILES(String ptofile) {
        }
    }

}

package com.chemgenerator.converter.task;

import com.chemgenerator.task.Task;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Component(value = "tableConverter")
public class TableConverter implements Converter<Task.Table, List<List<String>>> {

    @Override
    public List<List<String>> convert(Task.Table table) {
        List<List<String>> convertedTable = new LinkedList<>();
        for (Map.Entry<Integer, Map<Integer, StringBuilder>> entry : table.getTableContent().entrySet()) {
            List<String> row = new LinkedList<>();
            convertedTable.add(row);
            for (Map.Entry<Integer, StringBuilder> columnEntry : entry.getValue().entrySet()) {
                row.add(columnEntry.getValue().toString());
            }
        }
        return convertedTable;
    }
}

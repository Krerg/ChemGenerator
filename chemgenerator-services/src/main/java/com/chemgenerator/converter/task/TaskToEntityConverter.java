package com.chemgenerator.converter.task;

import com.chemgenerator.mongodb.enitites.TaskEntity;
import com.chemgenerator.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author amylnikov
 */
@Component(value = "taskToEntityConverter")
public class TaskToEntityConverter implements Converter<Task, TaskEntity> {

    @Autowired
    @Qualifier(value = "tableConverter")
    private Converter<Task.Table, List<List<String>>> tableConverter;

    @Override
    public TaskEntity convert(Task task) {
        TaskEntity entityTask = new TaskEntity();
        entityTask.setCondition(task.getCondition());
        entityTask.setGroupVariant(String.valueOf(task.getGroupVariant()));
        entityTask.setTaskVariant(String.valueOf(task.getTaskVariant()));
        entityTask.setTaskTable(tableConverter.convert(task.getTable()));
        return entityTask;
    }

}

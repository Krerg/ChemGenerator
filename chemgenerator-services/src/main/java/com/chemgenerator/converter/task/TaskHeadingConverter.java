package com.chemgenerator.converter.task;

import com.chemgenerator.mongodb.enitites.AvailableTaskEntity;
import com.chemgenerator.task.TaskRelatedValuesStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * @author amylniko
 */
@Component(value = "taskHeadingConverter")
public class TaskHeadingConverter implements Converter<AvailableTaskEntity, AvailableTaskEntity> {

    @Autowired
    @Qualifier(value = "defaultTaskRelatedValuesStorage")
    private TaskRelatedValuesStorage taskRelatedValuesStorage;

    @Override
    public AvailableTaskEntity convert(AvailableTaskEntity availableTaskEntity) {
        String groupVariant = availableTaskEntity.getGroupVariant();
        availableTaskEntity
                .setGroupVariant(taskRelatedValuesStorage.getRelatedValue(null, availableTaskEntity.getGroupVariant()));
        availableTaskEntity
                .setTaskVariants(availableTaskEntity
                        .getTaskVariants().stream().map(taskVariant -> taskRelatedValuesStorage
                                .getRelatedValue(taskVariant, groupVariant))
                        .collect(Collectors.toList()));
        return availableTaskEntity;
    }

}

package com.chemgenerator.exception;

import java.nio.file.Path;

/**
 * Exception, which could be threw when task file
 * doesn't math pattern
 *
 * @see com.chemgenerator.service.impl.ParserServiceImpl#parseTaskFile(Path)
 * @author amylnikov
 */
public class IncorrectFileFormatException extends Exception {

    /**
     * Shows which line doesn't match pattern
     * (could be 0 if whole file doesn't match pattern)
     */
    private int line;

    /**
     * Message, that tells why this file doesn't match pattern
     */
    private String message;

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

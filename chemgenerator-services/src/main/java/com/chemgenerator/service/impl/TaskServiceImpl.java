package com.chemgenerator.service.impl;

import com.chemgenerator.config.ConfigCV;
import com.chemgenerator.config.ConfigService;
import com.chemgenerator.exception.NoTasksToProvideException;
import com.chemgenerator.model.TaskBundleDTO;
import com.chemgenerator.mongodb.enitites.*;
import com.chemgenerator.mongodb.queue.impl.ProvideTaskQueue;
import com.chemgenerator.mongodb.repos.FileStorage;
import com.chemgenerator.mongodb.repos.TaskBundleRepository;
import com.chemgenerator.mongodb.repos.TaskRepository;
import com.chemgenerator.mongodb.repos.UserRepository;
import com.chemgenerator.rest.pojo.request.ProvideTaskPojo;
import com.chemgenerator.service.DocumentService;
import com.chemgenerator.service.TaskService;
import com.chemgenerator.task.ProvideTaskCallable;
import com.chemgenerator.task.Task;
import com.chemgenerator.task.TaskRelatedValuesStorage;
import com.chemgenerator.utils.TaskBundleUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * @author amylnikov
 */
@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskBundleRepository taskBundleRepository;

    private ExecutorService executorService;

    @Autowired
    private ProvideTaskQueue provideTaskQueue;

    @Autowired
    @Qualifier("wordService")
    private DocumentService documentService;

    @Autowired
    private FileStorage fileStorage;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    @Qualifier(value = "taskToEntityConverter")
    private Converter<Task, TaskEntity> taskConverter;

    @Autowired
    @Qualifier(value = "taskHeadingConverter")
    private Converter<AvailableTaskEntity,AvailableTaskEntity> taskHeadingConverter;

    @Autowired
    private TaskRelatedValuesStorage taskRelatedValuesStorage;

    @Autowired
    private ConfigService configService;

    @Autowired
    private ApplicationContext applicationContext;

    @PostConstruct
    public void init() {
        executorService = Executors
                .newFixedThreadPool((int)configService.getConfig(ConfigCV.THREAD_POOL_THREAD_CAPACITY));
        for (int i = 0; i < (int)configService.getConfig(ConfigCV.THREAD_POOL_THREAD_CAPACITY); i++) {
            Callable callable = applicationContext.getBean(ProvideTaskCallable.class);
            executorService.submit(callable);
        }
    }

    @Override
    public void saveTasks(List<Task> tasks) {
        List<TaskEntity> entityTasks = tasks.stream().map(task -> taskConverter.convert(task))
                .collect(Collectors.toList());
        taskRepository.parallelSave(entityTasks);
    }

    @Override
    public List<AvailableTaskEntity> getAvailableTasks() {
        List<AvailableTaskEntity> availableTasks =taskRepository.getAvailableTasks();
        availableTasks.forEach(task -> taskHeadingConverter.convert(task));
        return availableTasks;
    }

    @Override
    public List<AvailableTaskEntity> getUserAvailableTaskGroups() {
        return null;
    }

    @Override
    public void submitOrder(String username,
            List<ProvideTaskPojo> tasksToProvide,
            int numberOfCopies,
            String bundleName) {
        ProvideTasksMessage message = new ProvideTasksMessage(tasksToProvide, numberOfCopies, bundleName, username);
        message.setQueueName(ProvideTasksMessage.QUEUE_NAME);
        provideTaskQueue.send(message);
    }

    @Override
    public void provideTasksToUser(String username,
            List<ProvideTaskPojo> tasksToProvide,
            int numberOfVariants,
            String bundleName) throws NoTasksToProvideException {
        UserTaskBundles userTaskBundles = userRepository.findByLogin(username).getUserBundles();
        TaskBundle taskBundle = assembleTaskBundle(tasksToProvide, numberOfVariants);
        TaskBundleDTO taskBundleDocuments = documentService.generateTaskBundle(taskBundle.getTasks(), bundleName);
        saveFiles(taskBundleDocuments);
        taskBundle.setBundleFileNameWithAnswers(taskBundleDocuments.fileWithAnswersName);
        taskBundle.setBundleFileNameWithoutAnswers(taskBundleDocuments.fileWithoutAnswersName);
        userTaskBundles.getTaskBundles().add(taskBundle);
        taskBundleRepository.save(userTaskBundles);
        deleteTasksFromRepository(taskBundle);
    }

    private void saveFiles(TaskBundleDTO taskBundle) {
        fileStorage.save(taskBundle.fileWithAnswersData, taskBundle.fileWithAnswersName);
        fileStorage.save(taskBundle.fileWithoutAnswersData, taskBundle.fileWithoutAnswersName);
    }

    private void deleteTasksFromRepository(TaskBundle taskBundle) {
        for(TaskVariant variant : taskBundle.getTasks()) {
            for(TaskEntity task : variant.getTasks()) {
                taskRepository.delete(task);
            }
        }
    }

    private TaskBundle assembleTaskBundle(List<ProvideTaskPojo> tasksToProvide, int numberOfVariants) throws NoTasksToProvideException {
        TaskBundle taskBundle = new TaskBundle();
        taskBundle.setTasks(new LinkedList<>());
        for(int i = 0; i < numberOfVariants; i++) {
            TaskVariant variant = new TaskVariant();
            taskBundle.getTasks().add(variant);
            for (ProvideTaskPojo requestPojo : tasksToProvide) {
                TaskEntity taskEntity;
                while (true) {
                    taskEntity = getTaskFromRepository(requestPojo.getGroupVariant(), requestPojo.getTaskVariant());
                    if (taskEntity != null) {
                        break;
                    } else {
                        throw new NoTasksToProvideException();
                    }
                }
                variant.getTasks().add(taskEntity);
            }
        }
        return taskBundle;
    }

    private TaskEntity getTaskFromRepository(String groupVariant, String taskVariant) {
        String groupVariantId = taskRelatedValuesStorage.getTaskIdFromName(groupVariant);
        String taskVariantId = taskRelatedValuesStorage.getTaskIdFromName(taskVariant);
        return taskRepository.markAndGetTask(groupVariantId, taskVariantId);
    }

}

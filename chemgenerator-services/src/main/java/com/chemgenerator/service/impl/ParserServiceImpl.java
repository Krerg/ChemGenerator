package com.chemgenerator.service.impl;

import com.chemgenerator.service.TaskService;
import com.chemgenerator.task.Task;
import com.chemgenerator.task.TasksBuilder;
import com.chemgenerator.config.ConfigCV;
import com.chemgenerator.config.ConfigService;
import com.chemgenerator.exception.IncorrectFileFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Implementation for parser service. Lookups for text file with chem tasks
 * (asynchronously)
 *
 * @author amylnikov
 */
@Service
public class ParserServiceImpl {

    private static Logger LOGGER = LoggerFactory.getLogger(ParserServiceImpl.class);

    @Autowired
    private ConfigService configService;

    private static Path lookUpFolder;

    @Autowired
    private TaskService taskService;

    @PostConstruct
    public void setUpService() {
        lookUpFolder = Paths.get(String.valueOf(configService.getConfig(ConfigCV.LOOKUP_FOLDER_PATH)));
    }

    @Scheduled(fixedRate = 5000)
    private void startFileLookup() throws InterruptedException {
        try {
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(lookUpFolder)) {
                for (Path entry : stream) {
                    if (!Files.isDirectory(entry)) {
                        LOGGER.info("Find file in task folder:" + entry.getFileName().toString());
                        boolean parseResult = parseTaskFile(entry);
                        if (parseResult) {
                            entry.toFile().delete();
                        } else {

                        }
                    }
                }
            }
        } catch (IOException e) {
            // TODO reconfigure lookup folder
        }
    }

    /**
     * Parses task file and write result into DB.
     *
     * @param taskFile
     *            File with chem tasks
     * @return true if parsing is complete successful, otherwise false
     */
    private boolean parseTaskFile(Path taskFile) {
        String fileName = taskFile.getFileName().toString();
        String fileExtension = fileName.substring(fileName.lastIndexOf("."));
        if (!".txt".equals(fileExtension)) {
            return false;
        }
        try (BufferedReader fileReader = new BufferedReader(new FileReader(taskFile.toFile()))) {
            String descriptionLine = fileReader.readLine();
            // TODO add description line validator

            TasksBuilder tasksBuilder = new TasksBuilder();
            fileReader.lines().forEach(s -> {
                try {
                    tasksBuilder.addLine(s);
                } catch (IncorrectFileFormatException e) {
                    // TODO Log error
                    LOGGER.error(
                            "Error while parsing task file on line " + e.getLine() + " with cause:" + e.getMessage());
                    throw new IllegalArgumentException();
                }
            });
            List<Task> tasks = tasksBuilder.build();
            taskService.saveTasks(tasks);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static void reconfigure() {

    }

    private void moveToErrorFolder(Path file) {

    }

}

package com.chemgenerator.service.impl;

import com.chemgenerator.config.ConfigCV;
import com.chemgenerator.config.ConfigService;
import com.chemgenerator.model.TaskBundleDTO;
import com.chemgenerator.model.collections.Pair;
import com.chemgenerator.mongodb.enitites.TaskEntity;
import com.chemgenerator.mongodb.enitites.TaskVariant;
import com.chemgenerator.service.DocumentService;
import org.apache.pdfbox.text.TextPosition;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xwpf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;


/**
 *
 * Implementation {@link DocumentService}.
 * Generates files in docx format (Microsoft Word 2007)
 *
 * @author amylnikov
 */
@Service(value = "wordService")
public class WordServiceImpl implements DocumentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WordServiceImpl.class);

    @Autowired
    private ConfigService configService;

    /**
     * {@inheritDoc}
     */
    public TaskBundleDTO generateTaskBundle(List<TaskVariant> tasks, String bundleName) {
        try {
            String pathPrefix = (String) configService.getConfig(ConfigCV.TASK_BUNDLE_DIRECTORY);
            String fileNameWithExtension1 = bundleName+" - Без ответов"+".docx";
            String fileNameWithExtension2 = bundleName+" - С ответами"+".docx";
            String fileNameWithoutAnswers = pathPrefix + fileNameWithExtension1;
            String fileNameWithAnswers = pathPrefix + fileNameWithExtension2;
            byte[] fileWithAnswersData = createDocument(fileNameWithAnswers,tasks,true);
            LOGGER.info("Successfully generated task bundle file:" + fileNameWithoutAnswers);
            byte[] fileNameWithoutAnswersData = createDocument(fileNameWithoutAnswers,tasks,false);
            LOGGER.info("Successfully generated task bundle file:" + fileNameWithAnswers);
            TaskBundleDTO taskBundleDTO = new TaskBundleDTO(fileNameWithExtension2, fileNameWithExtension1,
                    fileWithAnswersData, fileNameWithoutAnswersData);
            return taskBundleDTO;
        } catch (IOException e) {
            LOGGER.error("Error while generating task bundle file:" + "df.docx" +
                    "Exception message:"+e.getMessage());
        }
        return null;
    }

    private byte[] createDocument(String fileName, List<TaskVariant> variants, boolean showAnswers) throws IOException {
        ByteArrayOutputStream out;
        byte[] bytes;
        try (XWPFDocument wordDocument = new XWPFDocument()) {
            out = new ByteArrayOutputStream();
            for (TaskVariant taskVariant : variants) {
                XWPFParagraph headerParagraph = wordDocument.createParagraph();
                headerParagraph.setPageBreak(true);
                addVariantHeader(headerParagraph, taskVariant.getVariantName());
                XWPFParagraph conditionParagraph;
                for(TaskEntity task : taskVariant.getTasks()) {
                    conditionParagraph = wordDocument.createParagraph();
                    addFormattedTextToParagraph(conditionParagraph, task.getCondition());
                    fillTable(wordDocument.createTable(), task, showAnswers);
                }
            }
            wordDocument.write(out);
        }
        bytes = out.toByteArray();
        out.close();
        return bytes;
    }

    private void addVariantHeader(XWPFParagraph header, String variantName) {
        header.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun headerRun = header.createRun();
        headerRun.setBold(true);
        headerRun.setFontSize(16);
        headerRun.setText(variantName);
    }

    /**
     * Fills table with formatted text from tasks.
     * @param table target table
     * @param taskEntity tasks
     * @param showAnswers if true, then only first column will be filled
     */
    private void fillTable(XWPFTable table, TaskEntity taskEntity, boolean showAnswers) {
        boolean firstRow = true;
        for (List<String> row : taskEntity.getTaskTable()) {
            XWPFTableRow tableRow;
            if(!firstRow) {
                tableRow = table.createRow();
            } else {
                tableRow = table.getRow(0);
            }

            int columnCount = 0;
            for(String column : row) {
                XWPFTableCell cell;
                if(firstRow && columnCount==0) {
                    cell = tableRow.getCell(0);
                } else if(firstRow && columnCount!=0) {
                    cell = tableRow.addNewTableCell();
                } else {
                    cell = tableRow.getCell(columnCount);
                }
                fillCell(cell,column);
                columnCount++;
                if(!showAnswers && !firstRow) {
                    break;
                }
            }
            firstRow = false;
        }
    }

    private void fillCell(XWPFTableCell cell, String text) {
        addFormattedTextToParagraph(cell.addParagraph(),text);
    }

    /**
     * Fills word paragraph with html formatted text
     * (e.g. replaces <sub>...</sub> and <sup>...</sup> tags)
     * @param paragraph paragraph in which text will be setted (couldn't be null)
     * @param text formatted text
     */
    private void addFormattedTextToParagraph(XWPFParagraph paragraph, String text) {
        boolean tagReading = false;
        XWPFRun currentParagraph = null;
        String tag = "";
        for(char c : text.toCharArray()) {
            if(c == '<') {
                tagReading = true;
                tag = "";
                continue;
            }
            if(c == '>') {
                tagReading = false;
                if(tag.equalsIgnoreCase("sup")) {
                    currentParagraph = paragraph.createRun();
                    currentParagraph.setFontSize(10);
                    currentParagraph.setSubscript(VerticalAlign.SUPERSCRIPT);
                    continue;
                } else if (tag.equalsIgnoreCase("sub")) {
                    currentParagraph = paragraph.createRun();
                    currentParagraph.setFontSize(10);
                    currentParagraph.setSubscript(VerticalAlign.SUBSCRIPT);
                    continue;
                } else if(tag.contains("/")) {
                    currentParagraph = paragraph.createRun();
                    currentParagraph.setFontSize(12);
                }
                continue;
            }
            if(tagReading) {
                tag+=c;
                continue;
            }
            if(currentParagraph == null) {
                currentParagraph = paragraph.createRun();
            }
            currentParagraph.setText(String.valueOf(c),currentParagraph.getTextPosition());
        }
    }
}


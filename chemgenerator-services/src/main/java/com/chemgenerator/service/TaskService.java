package com.chemgenerator.service;

import com.chemgenerator.exception.NoTasksToProvideException;
import com.chemgenerator.mongodb.enitites.AvailableTaskEntity;
import com.chemgenerator.rest.pojo.request.ProvideTaskPojo;
import com.chemgenerator.rest.pojo.response.TaskPojo;
import com.chemgenerator.task.Task;

import java.util.List;

/**
 * @author amylnikov
 */
public interface TaskService {

    void saveTasks(List<Task> tasks);

    List<AvailableTaskEntity> getAvailableTasks();

    List<AvailableTaskEntity> getUserAvailableTaskGroups();

    void submitOrder(String username, List<ProvideTaskPojo> tasksToProvide, int numberOfCopies, String bundleName);

    void provideTasksToUser(String username, List<ProvideTaskPojo> tasksToProvide, int numberOfCopies, String bundleName) throws NoTasksToProvideException;

}

package com.chemgenerator.service;

import com.chemgenerator.model.TaskBundleDTO;
import com.chemgenerator.model.collections.Pair;
import com.chemgenerator.mongodb.enitites.TaskEntity;
import com.chemgenerator.mongodb.enitites.TaskVariant;

import java.util.List;

/**
 * @author amylnikov
 */
public interface DocumentService {

    /**
     * Generates 2 files with tasks (with answers and without).
     * @param tasks list of tasks
     * @return DTO with filenames and its binary data
     */
    TaskBundleDTO generateTaskBundle(List<TaskVariant> tasks, String bundleName);

}

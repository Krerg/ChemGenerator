package com.chemgenerator.mail;

public interface MailService {

    void sendMessage(String subject, String text);

}
